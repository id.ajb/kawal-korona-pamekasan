<?php

use Illuminate\Support\Facades\DB;

function age($tanggal_lahir) {
    $date = date_create($tanggal_lahir);
    $newdate = '0000-00-00';
    if($date){
        $newdate = date_format($date,"Y-m-d");
    }
    // return $newdate;
    list($year,$month,$day) = explode("-",$newdate);
    $year_diff  = date("Y") - $year;
    $month_diff = date("m") - $month;
    $day_diff   = date("d") - $day;
    if ($month_diff < 0) $year_diff--;
        elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
    return $year_diff;
}

function fdate($date,$format='d M, Y'){
    return ($date) ? date_format(date_create((date_create($date))? $date: date('Y-m-d')), $format) : '000-00-00';
}
