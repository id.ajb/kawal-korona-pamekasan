<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KontakModels as kontak;
class KontakController extends Controller
{
    public function getKontak($id=null){
        if($id){
            $data['kontak'] = kontak::with('kecamatan')->where('id', $id)->first();
            return $data['kontak'];
        }else{
            $data['kontak'] = kontak::with('kecamatan')->get();
        }
        return view('components.kontak.index', $data);
    }

    public function setKontakGeneral(request $request){
        kontak::where('id', '!=', null)->update(['status'=>'kecamatan']);
        kontak::find($request->id)->update(['status'=>'general']);
        return redirect('kontak')->with('messge', ['status' => true, 'message' => 'Berhasil atur kontak umum']);
    }
    public function addKontak(request $request){
        $roles = [
            'kecamatan'     => "required|unique:kontak,district_id,NULL,id,deleted_at,NULL",
            'hotline'       => 'required|array|min:1',
            'hotline.*'     => 'required|numeric'
        ];
        if($request->call_center){
            $roles['call_center'] = 'numeric';
        }
        $request->validate($roles);

        $data = [
            'district_id'   => $request->kecamatan,
            'call_center'   => $request->call_center,
            'hotline'       => json_encode($request->hotline)
        ];

        return kontak::create($data);
    }

    public function editKontak(request $request){
        $check = kontak::findOrFail($request->id);
        if(!$check){
            return [
                'status'    => false,
                'message'   => 'Data tidak ditemukan'
            ];
        }
        $roles = [
            'hotline'       => 'required|array|min:1',
            'hotline.*'     => 'required|numeric'
        ];
        if($check->district_id != $request->kecamatan){
            $roles['kecamatan'] = 'required|unique:kontak,district_id';
        }
        if($request->call_center){
            $roles['call_center'] = 'numeric';
        }
        $request->validate($roles);

        $data = [
            'district_id'   => $request->kecamatan,
            'call_center'   => $request->call_center,
            'hotline'       => json_encode($request->hotline)
        ];

        return $check->update($data);
    }

    public function deleteKontak(request $request){
        $check = kontak::with('kecamatan')->where('id', $request->id)->first();
        $check->delete();
        return redirect('kontak')->with('messge', ['status' => true, 'message' => 'Data kontak kecamatan '.$check->kecamatan->name. ' Berhasil dihapus']);
    }

}
