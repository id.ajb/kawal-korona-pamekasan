<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;
use App\Models\NewsModels as News;

class NewsController extends Controller
{
    public function getNews(){
        $data['news'] = News::with('author')->get();
        return view('components.news.index', $data);
    }
    public function getFormNews($hiperlink = null){
        $news = News::where('link', $hiperlink)->limit(1)->first();
        $data['id']   = ($news)? $news->id : 'false';
        $data['data'] = $news;
        return view('components.news.add', $data);
    }
    public function getToken(){
        return 'a';
    }
    public function UploadImages(request $request){
        $file = $request->file('images_ajb');
        if($file){
            $file_name =  date('Y-m-d-his').'_'.$file->getClientOriginalName();
            $file_system = basename(Storage::disk('local')->put('public/images/news', $file));
            Storage::move('public/images/news/'.$file_system, 'public/images/news/'.$file_name);
        }
    }

    public function UpdateNews(request $request){
        if($request->title != false
        && $request->hiperlink != false
        && $request->contents != false){
            
            $data = [
                'img_thumb'     => $request->set_feature_images,
                'link'          => trim($request->hiperlink),
                'title'         => $request->title,
                'contents'      => $request->contents,
                'order'         => $request->id,
                'news_type'     => $request->news_status,
            ];
            
            $exists = News::where('link', $request->hiperlink)->first();
            $check = News::find($request->id);
            if($check){
                if($check->link != $request->hiperlink 
                && $exists){
                    return [
                        'status'    => false,
                        'message'   => 'Hiperlink sudah ada di database',
                    ];
                }
                $data['updated_by']     = auth()->user()->id;
                $data['updated_date']   = date('Y-m-d h:i:s');
                $check->update($data);

                return [
                    'status'    => true,
                    'message'   => 'Berita berhasil diperbarui',
                    'actions'   => $request->actions,
                    'id'        => $request->id,
                ];
            }else{
                if($exists){
                    if($exists->link == $request->hiperlink){
                        return [
                            'status'    => false,
                            'message'   => 'Hiperlink sudah ada di database'
                        ];
                    }
                }
                $data['added_by']       = auth()->user()->id;
                $data['created_date']   = date('Y-m-d h:i:s');
                $new = News::create($data);

                return [
                    'status'    => true,
                    'message'   => 'Berita berhasil ditambah',
                    'actions'   => $request->actions,
                    'id'        => $new->id
                ];
            }
            
        }else{
            return [
                'status'    => false,
                'message'   => 'Judul, Hiperlink, Atur gambar utama, dan isi berita tidak boleh kosong'
            ];
        }
    }

    public function GetImages(){
        $path = storage_path('app/public/images/news');
        $files = array_diff(scandir($path), array('.', '..'));
        $result = [];
        if($files){
            foreach ($files as $key => $value) {
                $result[] = [
                    'url' => url('images/news/'.$value),
                    'thumb' => url('images/news/'.$value),
                    'tag' => "beach",
                ];
            }
        }

        return $result;
    }
    public function checkAkun(request $request){
        if($request->id){
            $check = User::find($request->id);
            $roles = [
                'nama_lengkap'      => 'required',
                'password'          => 'required',
            ];
            if($check->email != $request->username){
                $roles['username'] = "required|unique:users,email";
            }
            $request->validate($roles);
        }else{
            $roles = [
                'nama_lengkap'      => 'required',
                'username'          => "required|unique:users,email",
                'password'          => 'required',
            ];
            $request->validate($roles);
        }
    }

    public function addAkun(request $request){
        $roles = [
            'nama_lengkap'      => 'required',
            'username'          => "required|unique:users,email",
            'password'          => 'required',
        ];
        $request->validate($roles);
        $file = $request->file('images');
        $file_name = '';
        if($file){
            $file_name =  date('Y-m-d-his').'_'.$file->getClientOriginalName();
            $file_system = basename(Storage::disk('local')->put('public/images/users', $file));
            Storage::move('public/images/users/'.$file_system, 'public/images/users/'.$file_name);
        }

        $data = [
            'name'          => $request->nama_lengkap,
            'email'         => $request->username,
            'password'      => Hash::make($request->password),
            'image'         => $file_name,
            'district_id'   => $request->kecamatan,
            'village_id'    => $request->kelurahan,
            'level'         => ($request->level)? $request->level : 'mobile'
        ];

        User::create($data);
        return redirect('akun')->with('messge', ['status' => true, 'message'=> 'Akun '.$request->nama_lengkap. ' berhasil ditambahkan']);
    }

    public function editAkun(request $request){
        $check = User::findOrFail($request->id);
        if(!$check){
            return redirect('akun')->with('messge', ['status' => false, 'message'=> 'Data tidak ditemukan']);
        }
        $roles = [
            'nama_lengkap'      => 'required',
            'password'          => 'required',
        ];
        if($check->email != $request->username){
            $roles['username'] = "required|unique:users,email";
        }
        $request->validate($roles);

        $file = $request->file('images');
        $file_name = $check->image;
        if($file){
            $file_name =  date('Y-m-d-his').'_'.$file->getClientOriginalName();
            $file_system = basename(Storage::disk('local')->put('public/images/users', $file));
            Storage::move('public/images/users/'.$file_system, 'public/images/users/'.$file_name);
        }

        $data = [
            'name'          => $request->nama_lengkap,
            'email'         => $request->username,
            'password'      => Hash::make($request->password),
            'image'         => $file_name,
            'district_id'   => $request->kecamatan,
            'village_id'    => $request->kelurahan,
            'level'         => ($request->level)? $request->level : 'mobile'
        ];

        $check->update($data);
        return redirect('akun')->with('messge', ['status' => true, 'message'=> 'Akun '.$request->nama_lengkap. ' berhasil diperbarui']);
    }

    public function deleteAkun(request $request){
        $check = User::find($request->id);
        $check->delete();
        return redirect('akun')->with('messge', ['status' => true, 'message'=> 'Akun '.$check->name. 'Akun '.$check->name. ' Berhasil dihapus']);
    }

}
