<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;
use App\Models\NewsModels as News;
use App\Models\KontakModels as Kontak;
use App\Models\PasienModels as Pasien;

class BlogController extends Controller
{
    public function index($link = null){
        if($link){
            $data['blog'] = News::where('link', $link)->where('news_type', 'publish')->get()->first();
            return view('guest.blogs-detail', $data);
        }else{

            $cvd = Pasien::all();
            $odp = [];
            $pdp = [];
            $positif = [];
            $sembuh = [];
            $meninggal = [];
            if($cvd->count() > 0){
                foreach ($cvd as $key => $c) {
                    if($c->status_covid == 'ODP'){
                        $odp[] = 'y'; 
                    }elseif($c->status_covid == 'PDP'){
                        $pdp[] = 'y';
                    }elseif($c->status_covid == 'POSITIF AKTIF'){
                        $positif[] = 'y';
                    }elseif($c->status_covid == 'POSITIF SEMBUH'){
                        $sembuh[] = 'y';
                    }elseif($c->status_covid == 'MENIGGAL'){
                        $meninggal[] = 'y';
                    }
                }
            }

            $data['covid']  = [
                'odp'   => count($odp),
                'pdp'   => count($pdp),
                'positif'   => count($positif),
                'sembuh'   => count($sembuh),
                'meninggal'   => count($meninggal),
            ];
            // return $data['covid'];
            $data['general']    = Kontak::where('status', 'general')->first();
            // return $data['general'];
            $data['blog']   = News::where('news_type', 'publish')->get();
            $data['kontak'] = Kontak::with('kecamatan')->get();
            return view('guest.welcome', $data);
        }
    }

    public function preview($link = null){
        $data['blog'] = News::where('link', $link)->get()->first();
        return view('guest.blogs-detail', $data);
    }
    

}
