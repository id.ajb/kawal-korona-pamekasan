<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PasienModels as Pasien;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $cvd = Pasien::all();
        $odp = [];
        $pdp = [];
        $positif = [];
        $sembuh = [];
        $meninggal = [];
        if($cvd->count() > 0){
            foreach ($cvd as $key => $c) {
                if($c->status_covid == 'ODP'){
                    $odp[] = 'y'; 
                }elseif($c->status_covid == 'PDP'){
                    $pdp[] = 'y';
                }elseif($c->status_covid == 'POSITIF AKTIF'){
                    $positif[] = 'y';
                }elseif($c->status_covid == 'POSITIF SEMBUH'){
                    $sembuh[] = 'y';
                }elseif($c->status_covid == 'MENIGGAL'){
                    $meninggal[] = 'y';
                }
            }
        }

        $data['covid']  = [
            'odp'   => count($odp),
            'pdp'   => count($pdp),
            'positif'   => count($positif),
            'sembuh'   => count($sembuh),
            'meninggal'   => count($meninggal),
        ];
        return view('home',$data);
    }
}
