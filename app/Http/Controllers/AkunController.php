<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class AkunController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    public function getAkun($id=null){
        if($id){
            $data['users'] = User::with('kecamatan')->with('kelurahan')->where('id', $id)->first();
            return $data['users'];
        }else{
            $data['users'] = User::with('kecamatan')->with('kelurahan')->get();
        }

        // return $data;
        return view('components.akun.index', $data);
    }
    public function checkAkun(request $request){
        if($request->id){
            $check = User::find($request->id);
            $roles = [
                'nama_lengkap'      => 'required',
                'password'          => 'required',
            ];
            if($check->email != $request->username){
                $roles['username'] = "required|unique:users,email";
            }
            $request->validate($roles);
        }else{
            $roles = [
                'nama_lengkap'      => 'required',
                'username'          => "required|unique:users,email",
                'password'          => 'required',
            ];
            $request->validate($roles);
        }
    }

    public function addAkun(request $request){
        $roles = [
            'nama_lengkap'      => 'required',
            'username'          => "required|unique:users,email",
            'password'          => 'required',
        ];
        $request->validate($roles);
        $file = $request->file('images');
        $file_name = '';
        if($file){
            $file_name =  date('Y-m-d-his').'_'.$file->getClientOriginalName();
            $file_system = basename(Storage::disk('local')->put('public/images/users', $file));
            Storage::move('public/images/users/'.$file_system, 'public/images/users/'.$file_name);
        }

        $data = [
            'name'          => $request->nama_lengkap,
            'email'         => $request->username,
            'password'      => Hash::make($request->password),
            'image'         => $file_name,
            'district_id'   => $request->kecamatan,
            'village_id'    => $request->kelurahan,
            'level'         => ($request->level)? $request->level : 'mobile'
        ];

        User::create($data);
        return redirect('akun')->with('messge', ['status' => true, 'message'=> 'Akun '.$request->nama_lengkap. ' berhasil ditambahkan']);
    }

    public function editAkun(request $request){
        $check = User::findOrFail($request->id);
        if(!$check){
            return redirect('akun')->with('messge', ['status' => false, 'message'=> 'Data tidak ditemukan']);
        }
        $roles = [
            'nama_lengkap'      => 'required',
            'password'          => 'required',
        ];
        if($check->email != $request->username){
            $roles['username'] = "required|unique:users,email";
        }
        $request->validate($roles);

        $file = $request->file('images');
        $file_name = $check->image;
        if($file){
            $file_name =  date('Y-m-d-his').'_'.$file->getClientOriginalName();
            $file_system = basename(Storage::disk('local')->put('public/images/users', $file));
            Storage::move('public/images/users/'.$file_system, 'public/images/users/'.$file_name);
        }

        $data = [
            'name'          => $request->nama_lengkap,
            'email'         => $request->username,
            'password'      => Hash::make($request->password),
            'image'         => $file_name,
            'district_id'   => $request->kecamatan,
            'village_id'    => $request->kelurahan,
            'level'         => ($request->level)? $request->level : 'mobile'
        ];

        $check->update($data);
        return redirect('akun')->with('messge', ['status' => true, 'message'=> 'Akun '.$request->nama_lengkap. ' berhasil diperbarui']);
    }

    public function deleteAkun(request $request){
        $check = User::find($request->id);
        $check->delete();
        return redirect('akun')->with('messge', ['status' => true, 'message'=> 'Akun '.$check->name. 'Akun '.$check->name. ' Berhasil dihapus']);
    }

}
