<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PasienModels as Pasien;
use App\Models\DistrictModels as District;
use App\Models\VillageModels as Village;
class PasienController extends Controller
{
    public function getPasien($id=null){
        if($id){
            $data['pasien'] = Pasien::with('kelurahan')->where('id', $id)->first();
            return $data['pasien'];
        }else{
            $data['pasien'] = Pasien::with('kelurahan')->get();
        }

        return view('components.pasien.index', $data);
    }
    public function getPasienByWilayah(request $request, $id=null){
        $data['wilayah']    = District::where('city_id', 3528)->with('pasien')->get();
        $data['label']      = 'Kecamatan';
        $data['filter']     = 'kec_id';
        $data['breakdown']  = true;
        if($request->kec_id){
            $data['label']      = 'Kelurahan';
            $data['filter']     = 'kel_id';
            $data['wilayah']    = Village::where('district_id', $request->kec_id)->with('pasien')->get();
        }
        if($request->kel_id){
            $data['breakdown']  = false;
            $data['label']      = 'Pasien';
            $data['filter']     = 'kel_id';
            $data['pasien']    = Pasien::where('village_id', $request->kel_id)->get();
        }

        return view('components.pasien.pasien-wilayah', $data);
    }

    public function getApiPasienByWilayah(request $request, $id=null){
        $data['wilayah']    = District::where('city_id', 3528)->with('pasien')->get();
        $data['label']      = 'Kecamatan';
        $data['filter']     = 'kec_id';
        $data['breakdown']  = true;
        if($request->kec_id){
            $data['label']      = 'Kelurahan';
            $data['filter']     = 'kel_id';
            $data['wilayah']    = Village::where('district_id', $request->kec_id)->with('pasien')->get();
        }
        if($request->kel_id){
            $data['breakdown']  = false;
            $data['label']      = 'Pasien';
            $data['filter']     = 'kel_id';
            $data['pasien']    = Pasien::where('village_id', $request->kel_id)->get();
        }

        return $data;
    }
    
    public function checkPasien(request $request){
        if($request->id){
            $check = Pasien::findOrFail($request->id);
            if(!$check){
                return [
                    'status'    => false,
                    'message'   => 'Data tidak ditemukan',
                ];
            }
            $roles = [
                'kecamatan'         => 'required',
                'kelurahan'         => 'required',
                'jenis_kelamin'     => "required",
                'status_covid'      => 'required',
            ];
            if($check->nik != $request->nik){
                $roles['nik']   = 'required|unique:pasien,nik';
            }else{
                $roles['nik']   = 'required';
            }
            $request->validate($roles);
        }else{
            $roles = [
                'kecamatan'         => 'required',
                'kelurahan'         => 'required',
                'nik'               => 'required|unique:pasien,nik',
                'jenis_kelamin'     => "required",
                'status_covid'      => 'required',
            ];
            $request->validate($roles);
        }
    }

    public function addPasien(request $request){
        $roles = [
            'kecamatan'         => 'required',
            'kelurahan'         => 'required',
            'nik'               => 'required|unique:pasien,nik',
            'jenis_kelamin'     => "required",
            'status_covid'      => 'required',
        ];
        $request->validate($roles);

        $data = [
            'name'              => $request->nama_lengkap,
            'nik'               => $request->nik,
            'gender'            => $request->jenis_kelamin,
            'place_of_birth'    => $request->tempat_lahir,
            'date_of_birth'     => $request->tanggal_lahir,
            'age'               => $request->usia,
            'district_id'       => $request->kecamatan,
            'village_id'        => $request->kelurahan,
            'added_by'          => auth()->user()->id,
            'status_covid'      => $request->status_covid,
        ];

        Pasien::create($data);
        return redirect($request->redirect)->with('messge', ['status' => true, 'message'=> 'Pasien '.$request->nama_lengkap. ' berhasil ditambahkan']);
    }

    public function editPasien(request $request){
        // return $request->all();
        $check = Pasien::findOrFail($request->id);
        if(!$check){
            return redirect('pasien')->with('messge', ['status' => false, 'message'=> 'Data tidak ditemukan']);
        }
        $roles = [
            'kecamatan'         => 'required',
            'kelurahan'         => 'required',
            'jenis_kelamin'     => "required",
            'status_covid'      => 'required',
        ];
        if($check->nik != $request->nik){
            $roles['nik']   = 'required|unique:pasien,nik';
        }else{
            $roles['nik']   = 'required';
        }
        $request->validate($roles);

        $data = [
            'name'              => $request->nama_lengkap,
            'nik'               => $request->nik,
            'gender'            => $request->jenis_kelamin,
            'place_of_birth'    => $request->tempat_lahir,
            'date_of_birth'     => $request->tanggal_lahir,
            'age'               => $request->usia,
            'district_id'       => $request->kecamatan,
            'village_id'        => $request->kelurahan,
            'added_by'          => auth()->user()->id,
            'status_covid'      => $request->status_covid,
        ];

        $check->update($data);
        return redirect('pasien')->with('messge', ['status' => true, 'message'=> 'Akun '.$request->nik. ' berhasil diperbarui']);
    }

    public function deletePasien(request $request){
        $check = Pasien::find($request->id);
        $check->delete();
        return redirect('pasien')->with('messge', ['status' => true, 'message'=> 'Pasien dengan NIK '.$check->nik. ' Berhasil dihapus']);
    }

}
