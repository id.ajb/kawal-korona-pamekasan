<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MasterImages extends Controller
{
    public function getImages($type, $path){
        if($path == 'null'){
            return [
                'status'    => false,
                'message'   => 'Image not found'
            ];
        }
        return Storage::get('public/images/'.$type.'/'.$path);
    }    
}
