<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AjaxWilayahController extends Controller
{
    public function getProvinces(){
        return \Indonesia::allProvinces();
    }

    public function getCities($id){
        return \Indonesia::findProvince($id, ['cities']);
    }

    public function getDistricts($id){
        return \Indonesia::findCity($id, ['districts']);
    }

    public function getVillages($id){
        return \Indonesia::findDistrict($id, ['villages']);
    }

    



}
