<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CityModels extends Model
{
    protected $table = 'indonesia_cities';
    protected $primaryKey = 'id';

}
