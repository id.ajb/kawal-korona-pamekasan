<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KontakModels extends Model
{
    use SoftDeletes;
    protected $table = 'kontak';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];


    public function kecamatan(){
        return $this->hasOne('App\Models\DistrictModels', 'id', 'district_id');
    }
}
