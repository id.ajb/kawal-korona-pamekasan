<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProvinsiModels extends Model
{
    protected $table = 'indonesia_provinces';
    protected $primaryKey = 'id';
}
