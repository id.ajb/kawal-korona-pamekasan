<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistrictModels extends Model
{
    protected $table = 'indonesia_districts';
    protected $primaryKey = 'id';

    public function pasien(){
        return $this->hasMany(PasienModels::class, 'district_id');
    }
}
