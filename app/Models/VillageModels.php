<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VillageModels extends Model
{
    protected $table = 'indonesia_villages';
    protected $primaryKey = 'id';

    public function pasien(){
        return $this->hasMany(PasienModels::class, 'village_id');
    }
}
