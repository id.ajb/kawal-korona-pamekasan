<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class PasienModels extends Model
{
    protected $table = 'pasien';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];


    public function kelurahan(){
        return $this->hasOne('App\Models\VillageModels', 'id', 'village_id');
    }

    public function added(){
        return $this->hasOne('App\User', 'id', 'added_by');
    }

}
