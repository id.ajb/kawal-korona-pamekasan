<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsModels extends Model
{
    use SoftDeletes;
    protected $table = 'news';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];


    public function author(){
        return $this->hasOne('App\User', 'id', 'added_by');
    }

    public function updated_by(){
        return $this->hasOne('App\User', 'id', 'updated_by');
    }

}
