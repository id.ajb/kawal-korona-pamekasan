<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePasienTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('pasien', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->id();
            $table->string('name')->nullable();
            $table->string('nik');
            $table->enum('gender',['L','P'])->default('L');
            $table->string('place_of_birth')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('age')->nullable();
            $table->char('district_id', 7);
            $table->char('village_id', 10);
            $table->enum('status_covid',['ODP','PDP','POSITIF AKTIF', 'POSITIF SEMBUH', 'MENIGGAL'])->default('ODP');
            $table->unsignedBigInteger('added_by')->unsigned();
            $table->timestamps();

            $table->foreign('added_by')->references('id')->on('users');
            $table->foreign('district_id')
                ->references('id')
                ->on(config('laravolt.indonesia.table_prefix').'districts')
                ->onUpdate('cascade')->onDelete('restrict');

            // $table->foreign('district')->references('id')->on('indonesia_districts');
            $table->foreign('village_id')
            ->references('id')
            ->on('indonesia_villages')
            ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasien');
    }
}
