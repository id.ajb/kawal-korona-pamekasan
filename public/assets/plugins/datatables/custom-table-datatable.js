/*
---------------------------------------
    : Custom - Table Datatable js :
---------------------------------------
*/
"use strict";
$(document).ready(function() {
    /* -- Table - Datatable -- */
    $('#datatable').DataTable({
        responsive: true
    });
    $('#default-datatable').DataTable( {
        "order": [[ 3, "desc" ]],
        responsive: true
    } );    
    var table = $('#datatable-buttons').DataTable({
        buttons: [
            {
                extend:    'copy',
                text:      '<i class="fa fa-files-o"></i> Copy',
                titleAttr: 'Copy',
                className: 'btn btn-outline-info ml-2 btn-rounded btn-sm'
            },
            {
                extend:    'csv',
                text:      '<i class="fa fa-files-o"></i> CSV',
                titleAttr: 'CSV',
                className: 'btn btn-outline-default ml-2 btn-rounded btn-sm',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend:    'excel',
                text:      '<i class="fa fa-files-o"></i> Excel',
                titleAttr: 'Excel',
                className: 'btn btn-outline-success ml-2 btn-rounded btn-sm',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend:    'pdf',
                text:      '<i class="fa fa-file-pdf-o"></i> PDF',
                titleAttr: 'PDF',
                className: 'btn btn-outline-danger ml-2 btn-rounded btn-sm',
                exportOptions: {
                    columns: ':visible'
                }
            },               
            {
                extend:    'print',
                text:      '<i class="fa fa-print"></i> Print',
                titleAttr: 'Print',
                className: 'btn btn-outline-warning ml-2 btn-rounded btn-sm',
                exportOptions: {
                    columns: ':visible'
                }
            },  
        ],
        lengthChange: true,
        responsive: true,
    });
    
    table.buttons().container().appendTo('#btn-print .focus');
});