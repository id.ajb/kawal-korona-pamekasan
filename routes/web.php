<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BlogController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('images/{type}/{path}', 'MasterImages@getImages');

Route::get('/provinsi', 'AjaxWilayahController@getProvinsi')->middleware('auth');
Route::get('/kabupaten/{id_prov}', 'AjaxWilayahController@getCities')->middleware('auth');
Route::get('/kecamatan/{id_kab}', 'AjaxWilayahController@getDistricts')->middleware('auth');
Route::get('/kelurahan/{id_kec}', 'AjaxWilayahController@getVillages')->middleware('auth');

Route::get('/kontak/{id?}', 'KontakController@getKontak')->middleware('auth');
Route::post('/add-kontak', 'KontakController@addKontak')->middleware('auth');
Route::post('/update-kontak', 'KontakController@editKontak')->middleware('auth');
Route::post('/set-general-kontak', 'KontakController@setKontakGeneral')->middleware('auth');
Route::post('/delete-kontak', 'KontakController@deleteKontak')->middleware('auth');


Route::get('/akun/{id?}', 'AkunController@getAkun')->middleware('auth');
Route::post('/check-akun', 'AkunController@checkAkun')->middleware('auth');
Route::post('/add-akun', 'AkunController@addAkun')->middleware('auth');
Route::post('/update-akun', 'AkunController@editAkun')->middleware('auth');
Route::post('/delete-akun', 'AkunController@deleteAkun')->middleware('auth');

Route::get('/pasien', 'PasienController@getPasien')->middleware('auth');
Route::get('/pasien-wilayah/{id?}', 'PasienController@getPasienByWilayah')->middleware('auth');
Route::get('/pasien/{id?}', 'PasienController@getPasien')->middleware('auth');
Route::post('/check-pasien', 'PasienController@checkPasien')->middleware('auth');
Route::post('/add-pasien', 'PasienController@addPasien')->middleware('auth');
Route::post('/update-pasien', 'PasienController@editPasien')->middleware('auth');
Route::post('/delete-pasien', 'PasienController@deletePasien')->middleware('auth');

Route::get('/news', 'NewsController@getNews')->middleware('auth');
Route::get('/get-berita', 'NewsController@getFormNews')->middleware('auth');
Route::get('/post-berita/{id?}', 'NewsController@getFormNews')->middleware('auth');
Route::post('/news-update', 'NewsController@UpdateNews')->middleware('auth');




Route::get('/get-pasien-by-wilayah', 'PasienController@getApiPasienByWilayah');
Route::get('/blog/{link?}', 'BlogController@index');
Route::get('/blog-preview/{link?}', 'BlogController@preview');