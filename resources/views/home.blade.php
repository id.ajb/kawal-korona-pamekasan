@extends('layouts.app')

@section('content')
<div class="container">
                    <div class="row">
                        <!-- Feature Block One -->
                        <div class="col-lg-4">
                            <div class="card border-danger" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <div class="card-body text-center">
                                    <!-- <div class="icon-box">
                                        <span class="icon"><img src="{{asset('assets/images/icons/1.png')}}" alt="" /></span>
                                    </div> -->
                                    <h3><a href="#" class="text-danger">POSITIF COVID-19</a></h3>
                                    <p><a href="#">PAMEKASAN</a></p>
                                    <div class="text-danger" style="font-size:50px">
                                        {{$covid['positif']}}
                                    </div>
                                    <!-- <div class="text">Hands touch many surfaces and can pick up viruses. Once contaminated, hands can transfer the virus.</div> -->
                                </div>
                            </div>
                        </div>
                        
                        <!-- Feature Block One -->
                        <div class="col-lg-4">
                            <div class="card border-success" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <div class="card-body text-center">
                                    <!-- <div class="icon-box">
                                        <span class="icon"><img src="{{asset('assets/images/icons/2.png')}}" alt="" /></span>
                                    </div> -->
                                    <h3><a href="#" class="text-success">SEMBUH</a></h3>
                                    <p><a href="#">PAMEKASAN</a></p>
                                    <div class="text-success" style="font-size:50px">
                                        {{$covid['sembuh']}}
                                    </div>
                                    <!-- <h3><a href="#">Air Transmission</a></h3>
                                    <div class="text">How easily a virus spreads from person-to-person can vary, Some viruses are highly contagious.</div> -->
                                </div>
                            </div>
                        </div>
                        
                        <!-- Feature Block One -->
                        <div class="col-lg-4">
                            <div class="card border-muted" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <div class="card-body text-center">
                                    <!-- <div class="icon-box">
                                        <span class="icon"><img src="{{asset('assets/images/icons/3.png')}}" alt="" /></span>
                                    </div> -->
                                    <h3><a href="#" class="text-muted">MENIGGAL</a></h3>
                                    <p><a href="#">PAMEKASAN</a></p>
                                    <div class="text-muted" style="font-size:50px">
                                        {{$covid['meninggal']}}
                                    </div>
                                    <!-- <h3><a href="#">Contaminated Objects</a></h3>
                                    <div class="text">Restaurants, grocery stores, food processing plants, even your own home—food contamination can happen.</div> -->
                                </div>
                            </div>
                        </div>				
                    </div>
        
                    <div class="row">
                        <!-- Feature Block One -->
                        
                        <!-- Feature Block One -->
                        <div class="col-lg-6">
                            <div class="card border-success">
                                <div class="card-body text-center">
                                    <h3><a href="#" class="text-success">ODP</a></h3>
                                    <div>Orang Dalam Pemantauan</div>
                                    <div class="row">
                                        <div class="col">
                                            <h5 class="text-success" style="font-weight:bold">~</h5>
                                            <p class="text">
                                                Proses Pemantauan
                                            </p>
                                        </div>
                                        <div class="col">
                                            <h5 class="text-success" style="font-weight:bold">~</h5>
                                            <p class="text">
                                                Selesai Pemantauan
                                            </p>
                                        </div>
                                        <div class="col">
                                            <h5 class="text-success" style="font-weight:bold">{{$covid['odp']}}</h5>
                                            <p class="text">
                                                Total ODP
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-6">
                            <div class="card border-warning">
                                <div class="card-body text-center">
                                    <h3><a href="#" class="text-warning">PDP</a></h3>
                                    <div class="text">Pasien Dalam Pengawasan</div>
                                    <div class="row">
                                        <div class="col">
                                            <h5 class="text-warning" style="font-weight:bold">~</h5>
                                            <p class="text">
                                                Proses Pemantauan
                                            </p>
                                        </div>
                                        <div class="col">
                                            <h5 class="text-warning" style="font-weight:bold">~</h5>
                                            <p class="text">
                                                Selesai Pemantauan
                                            </p>
                                        </div>
                                        <div class="col">
                                            <h5 class="text-warning" style="font-weight:bold">{{$covid['pdp']}}</h5>
                                            <p class="text">
                                                Total PDP
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>	
        
                    </div>
</div>
@endsection


@section('js')
    {{-- <script>
        $(document).ready(function(){
			$.ajax({
				url : '{{url("get-pasien-by-wilayah")}}',
				type : 'get',
				success : function(res){
					$('.table-wilayah').html('')
					var th = [
						'<tr>',
							'<th class="text-center">NO</th>',
							'<th>'+res.label+'</th>',
							'<th style="width:15%" class="text-center bg-light">ODP</th>',
							'<th style="width:15%" class="text-center bg-warning">PDP</th>',
							'<th style="width:15%" class="text-center text-white bg-danger">Positif Aktif</th>',
							'<th style="width:15%" class="text-center text-white bg-success">Positif Sembuh</th>',
							'<th style="width:15%" class="text-center text-white bg-secondary">Meninggal</th>',
						'</tr>'	
					].join('\n');
					$('.table-wilayah').html(th)					
					$.each(res.wilayah, function(index, val){
						var odp = [];
						var pdp = [];
						var positif = [];
						var sembuh = [];
						var meninggal = [];
						if(val.pasien != false){
							$.each(val.pasien, function(key, p){
								if(p.status_covid == 'ODP'){
									odp.push('y')
								} else if (p.status_covid == 'PDP') {
									pdp.push('y')
								} else if (p.status_covid == 'POSITIF AKTIF') {
									positif.push('y')
								} else if (p.status_covid == 'POSITIF SEMBUH') {
									sembuh.push('y')
								} else if (p.status_covid == 'MENIGGAL') {
									meninggal.push('y')
								}else{

								}
							})
							// console.log(odp.length);
						}
						var sumodp = (odp.length != 0)? odp.length : '-';
						var sumpdp = (pdp.length != 0)? pdp.length : '-';
						var sumpositif = (positif.length != 0)? positif.length : '-';
						var sumsembuh = (sembuh.length != 0)? sembuh.length : '-';
						var summeninggal = (meninggal.length != 0)? meninggal.length : '-';
						var tmp = [
							'<tr>',
								'<td class="text-center"> '+(index + 1)+' </td>',
								'<td><a link="{{url("get-pasien-by-wilayah")}}?'+res.filter+'='+val.id+'" class="link-data-pasien text-info" style="text-decoration:none"> '+val.name+' </a></td>',
								'<td class="text-center table-light"> '+ sumodp +' </td>',
								'<td class="text-center table-warning"> '+ sumpdp +' </td>',
								'<td class="text-center table-danger"> '+ sumpositif +' </td>',
								'<td class="text-center table-success"> '+ sumsembuh +' </td>',
								'<td class="text-center table-secondary"> '+ summeninggal +' </td>',
							'</tr>',
						].join('\n');
						$('.table-wilayah').append(tmp);

						var odp = [];
						var pdp = [];
						var positif = [];
						var sembuh = [];
						var meninggal = [];
					})


					$('.link-data-pasien').click(function(){
						var url = $(this).attr('link')

						
						$.ajax({
							url : url,
							type : 'get',
							success : function(res){
								$('.table-wilayah').html('')
								var th = [
									'<tr>',
										'<th class="text-center">NO</th>',
										'<th>'+res.label+'</th>',
										'<th style="width:15%" class="text-center bg-light">ODP</th>',
										'<th style="width:15%" class="text-center bg-warning">PDP</th>',
										'<th style="width:15%" class="text-center text-white bg-danger">Positif Aktif</th>',
										'<th style="width:15%" class="text-center text-white bg-success">Positif Sembuh</th>',
										'<th style="width:15%" class="text-center text-white bg-secondary">Meninggal</th>',
									'</tr>'	
								].join('\n');
								$('.table-wilayah').html(th)					
								$.each(res.wilayah, function(index, val){
									var odp = [];
									var pdp = [];
									var positif = [];
									var sembuh = [];
									var meninggal = [];
									if(val.pasien != false){
										$.each(val.pasien, function(key, p){
											if(p.status_covid == 'ODP'){
												odp.push('y')
											} else if (p.status_covid == 'PDP') {
												pdp.push('y')
											} else if (p.status_covid == 'POSITIF AKTIF') {
												positif.push('y')
											} else if (p.status_covid == 'POSITIF SEMBUH') {
												sembuh.push('y')
											} else if (p.status_covid == 'MENIGGAL') {
												meninggal.push('y')
											}else{

											}
										})
										// console.log(odp.length);
									}
									var sumodp = (odp.length != 0)? odp.length : '-';
									var sumpdp = (pdp.length != 0)? pdp.length : '-';
									var sumpositif = (positif.length != 0)? positif.length : '-';
									var sumsembuh = (sembuh.length != 0)? sembuh.length : '-';
									var summeninggal = (meninggal.length != 0)? meninggal.length : '-';
									var tmp = [
										'<tr>',
											'<td class="text-center"> '+(index + 1)+' </td>',
											'<td><a link="{{url("get-pasien-by-wilayah")}}?'+res.filter+'='+val.id+'" class="link-data-pasien" style="text-decoration:none"> '+val.name+' </a></td>',
											'<td class="text-center table-light"> '+ sumodp +' </td>',
											'<td class="text-center table-warning"> '+ sumpdp +' </td>',
											'<td class="text-center table-danger"> '+ sumpositif +' </td>',
											'<td class="text-center table-success"> '+ sumsembuh +' </td>',
											'<td class="text-center table-secondary"> '+ summeninggal +' </td>',
										'</tr>',
									].join('\n');
									$('.table-wilayah').append(tmp);

									var odp = [];
									var pdp = [];
									var positif = [];
									var sembuh = [];
									var meninggal = [];
								})


							}
						})


					})


				}
			})

			
			
		}) --}}
    </script>
@endsection