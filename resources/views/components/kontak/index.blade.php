@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-success">
                <div class="card-header bg-white">
                    <div class="float-right">
                        <button data-toggle="modal" data-target="#add" class="btn btn-sm btn-add btn-outline-success">
                            <i class="fa fa-plus"></i>
                            Tambah kontak
                        </button>
                    </div>
                    <i class="fa fa-phone mr-2" style="-webkit-transform: scaleX(-1);transform: scaleX(-1);"></i>
                    Kontak Center
                </div>

                <div class="card-body">
                    
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class=" text-center">NO</th>
                                    <th class="">Kecamatan</th>
                                    <th class="">Call Center</th>
                                    <th class="">Hotline</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($kontak->count() > 0)
                                    @foreach ($kontak as $key => $item)
                                        <tr>
                                            <td class="text-center"> {{ $key + 1 }} </td>
                                            <td> {{ $item->kecamatan->name }} </td>
                                            <td> {{($item->call_center)? $item->call_center : '-'}} </td>
                                            <td> 
                                                @foreach (json_decode($item->hotline, true) as $kontak)
                                                    <div class="btn btn-light">{{ $kontak }}</div>
                                                @endforeach
                                            </td>
                                            <td class="text-center">
                                                <button data-toggle="modal" data-target="#set_general" id="{{$item->id}}" class="btn btn-set-kontak btn-sm @if($item->status == 'general') btn-primary @else btn-light @endif">
                                                    <i class="fa fa-check"></i>
                                                </button>
                                                <button data-toggle="modal" data-target="#edit_kontak" id="{{$item->id}}" class="btn btn-edit btn-sm btn-success">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button data-toggle="modal" data-target="#delete" onclick="softDelete('{{$item->id}}','{{$item->kecamatan->name}}')" class="btn btn-sm btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<form action="" method="POST" class="form-add">
    @csrf
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-phone text-white bg-success" style="-webkit-transform: scaleX(-1);transform: scaleX(-1);border:solid 1px #38c172;padding:3px"></i>
                        Tambah kontak
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <select name="kecamatan" class="form-control"></select>
                        <small class="form-text kecamatan text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label>Call Center (opsional) </label>
                        <input name="call_center" type="number" class="form-control" placeholder="Call Center Number">
                        <small class="form-text call_center text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label>Hotline</label>

                        <div class="input-group mb-2">
                            <input name="hotline[]" type="number" class="form-control" placeholder="Phone Number">
                            <div class="input-group-prepend">
                                <button type="button" class="btn btn-multiple-kontak btn-outline-secondary"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="multiple-kontak"></div>
                        <small class="form-text hotline text-danger"></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="button" class="btn btn-save-kontak btn-success"> <i class="fa fa-save"></i> Simpan kontak</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form action="" method="POST" class="form-edit">
    @csrf
    <div class="modal fade" id="edit_kontak">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-pen text-white bg-success" style="border:solid 1px #38c172;padding:3px"></i>
                        Edit kontak
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <select name="kecamatan" class="form-control"></select>
                        <small class="form-text kecamatan text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label>Call Center (opsional) </label>
                        <input name="call_center" type="number" class="form-control" placeholder="Call Center Number">
                        <small class="form-text call_center text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label>Hotline</label>
                        <div class="multiple-kontak"></div>
                        <small class="form-text hotline text-danger"></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="button" class="btn btn-update-kontak btn-success"> <i class="fa fa-save"></i> Update kontak</button>
                </div>
            </div>
        </div>
    </div>
</form>


<form action="{{url('delete-kontak')}}" method="POST" class="form-delete">
    @csrf
    <div class="modal fade" id="delete">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-trash text-white bg-danger" style="border:solid 1px #e3342f;padding:3px"></i>
                        Hapus kontak
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <p class="text-center">
                        Yakin akan menghapus data kontak Kecamatan <span class="text-danger name"></span> ? 
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-update-kontak btn-success"> <i class="fa fa-trash"></i> Hapus kontak</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form action="{{url('set-general-kontak')}}" method="POST">
    @csrf
    <div class="modal fade" id="set_general">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-check text-white bg-primary" style="border:solid 1px blue;padding:3px"></i>
                        Kontak Umum
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <p class="text-center">
                        Kontak akan dijadikan Umum / General <span class="text-danger name"></span> ? 
                    </p>
                    <p>
                        <i class="text-muted">
                            Kontak Umum akan ditampilkan dibagian dashboard 
                        </small>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-update-kontak btn-success"> <i class="fa fa-cog"></i> Atur kontak</button>
                </div>
            </div>
        </div>
    </div>
</form>



@endsection



@section('js')

@if (\Session::has('messge'))
    <script>
        $(document).ready(function(){
            @if (\Session::get("messge")['status'] == true)
                toastr.success('{{\Session::get("messge")["message"]}}');
            @else
                toastr.error('{{\Session::get("messge")["message"]}}');                
            @endif
        })
    </script>
@endif


    <script>
        function softDelete(id, name){
            $('#delete .name').html(name);
            $('#delete input[name=id]').val(id);
        }
        $(document).ready(function(){
            $.ajax({
                url : '{{url("kecamatan/3528")}}',
                type: 'GET',
                success : function(res){
                    $('select[name=kecamatan]').html('<option value=""> -- Pilih Kecamatan -- </option>');
                    $.each(res.districts, function(index, val){
                        $('select[name=kecamatan]').append('<option value="'+val.id+'">'+val.name+'</option>');
                    })
                }
            })
            $('.btn-set-kontak').click(function(){
                var val = $(this).attr('id');
                $('#set_general input[name=id]').val(val);
            })
            $('.btn-add').click(function(){
                $('.multiple-kontak').html('');
            })
            $('.btn-edit').click(function(){
                var id = $(this).attr('id')
                $.ajax({
                    url : '{{url("kontak")}}/'+id,
                    type: 'get',
                    success : function(res){
                        $('#edit_kontak input[name=id]').val(res.id)
                        $('#edit_kontak select[name=kecamatan]').val(res.district_id)
                        $('#edit_kontak input[name=call_center]').val(res.call_center)
                        var kontak = $.parseJSON(res.hotline);
                        $('.multiple-kontak').html('');
                        $.each(kontak, function(index, val){
                            if(index == 0){
                                var button = '<button type="button" class="btn btn-multiple-kontak btn-outline-secondary"><i class="fa fa-plus"></i></button>';
                            }else{
                                var button = '<button type="button" class="btn remove-multiple-kontak btn-outline-danger"><i class="fa fa-times"></i></button>';
                            }
                            var formMoltiple = [
                                '<div class="input-group mb-2">',
                                    '<input name="hotline[]" value="'+val+'" type="number" class="form-control" placeholder="Phone Number">',
                                    '<div class="input-group-prepend">',
                                        button,
                                    '</div>',
                                '</div>',
                            ].join('\n');
                            $('.multiple-kontak').append(formMoltiple);
                        })
                        $('.btn-multiple-kontak').click(function(){
                            var formMoltiple = [
                                '<div class="input-group mb-2">',
                                    '<input name="hotline[]" type="number" class="form-control" placeholder="Phone Number">',
                                    '<div class="input-group-prepend">',
                                        '<button type="button" class="btn remove-multiple-kontak btn-outline-danger"><i class="fa fa-times"></i></button>',
                                    '</div>',
                                '</div>',
                            ].join('\n');
                            $('.multiple-kontak').append(formMoltiple);
                            $('.remove-multiple-kontak').click(function(){
                                $(this).closest('.input-group').remove();
                            })
                        })

                    }
                })
            })
            $('.btn-multiple-kontak').click(function(){
                var formMoltiple = [
                    '<div class="input-group mb-2">',
                        '<input name="hotline[]" type="number" class="form-control" placeholder="Phone Number">',
                        '<div class="input-group-prepend">',
                            '<button type="button" class="btn remove-multiple-kontak btn-outline-danger"><i class="fa fa-times"></i></button>',
                        '</div>',
                    '</div>',
                ].join('\n');
                $('.multiple-kontak').append(formMoltiple);
                $('.remove-multiple-kontak').click(function(){
                    $(this).closest('.input-group').remove();
                })
            })

            $('.btn-update-kontak').click(function(){
                $.ajax({
                    url : '{{url("update-kontak")}}',
                    type : 'post',
                    data : $('.form-edit').serialize(),
                    success : function(res){
                        if(res.status == false){
                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.error(res.message);
                            setTimeout(function(){
                                location.reload();
                            },2000)
                        }else{
                            $('.form-text').html('');
                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data berhasil ditambah');
                            setTimeout(function(){
                                location.reload();
                            },1000)
                        }
                    },
                    error : function(err){
                        $('.form-text').html('');
                        $.each(err.responseJSON.errors, function(index, val){
                            if(index.indexOf('.') > -1){
                                $('.'+index.slice(0,-2)).append(val[0]+'<br>');
                            }else{

                                $('.'+index).html(val[0]);
                            }
                            
                        })
                    }
                })
            })

            $('.btn-save-kontak').click(function(){
                $.ajax({
                    url : '{{url("add-kontak")}}',
                    type : 'post',
                    data : $('.form-add').serialize(),
                    success : function(res){
                        $('.form-text').html('');
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-bottom-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                        toastr.success('Data berhasil ditambah');
                        setTimeout(function(){
                            location.reload();
                        },1000)
                    },
                    error : function(err){
                        $('.form-text').html('');
                        $.each(err.responseJSON.errors, function(index, val){
                            if(index.indexOf('.') > -1){
                                $('.'+index.slice(0,-2)).append(val[0]+'<br>');
                            }else{

                                $('.'+index).html(val[0]);
                            }
                            
                        })
                    }
                })
            })
        })
        
    </script>
@endsection