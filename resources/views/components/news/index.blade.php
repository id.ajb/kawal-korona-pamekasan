@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-info">
                <div class="card-header bg-white">
                    <div class="float-right">
                        <a href="{{url('post-berita')}}" class="btn btn-sm btn-add btn-outline-success">
                            <i class="fa fa-plus"></i>
                            Buat Berita Baru
                        </a>
                    </div>
                    <i class="fa fa-newspaper mr-2"></i>
                    Akun users
                </div>

                <div class="card-body">
                    
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class=" text-center">NO</th>
                                    <th class="">Author</th>
                                    <th class="">Judul Berita</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($news->count() > 0)
                                    @foreach ($news as $key => $item)
                                        <tr>
                                            <td class="text-center" style="width:70px">
                                                <div class="@if ($item->level == 'super admin') border-light alert-light @elseif($item->level == 'admin') border-warning alert-warning @else border-success alert-success @endif" style="position:absolute;margin-top:-12px;margin-left:-12px;width:20px;height:20px;">
                                                    <div style="position:absolute;padding:0 5px;margin-top:-2px;">
                                                        {{ $key + 1 }} 
                                                    </div>
                                                </div>
                                                @if ($item->image)
                                                    <img style="width:40px;height:40px" class="img-thumbnail" src="{{url('images/users/'.$item->image)}}" alt="">
                                                @else
                                                    <img style="width:40px;height:40px" class="img-thumbnail" src="{{url('assets/images/gallery/users.svg')}}" alt="">                                                    
                                                @endif
                                            </td>
                                            <td>
                                                {{$item->author->name}}
                                            </td>
                                            <td>
                                                <div class="float-right">
                                                        <a href="{{url('post-berita/'.$item->link)}}" class="text-success">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <span style="cursor:pointer" data-toggle="modal" data-target="#delete" onclick="softDelete('{{$item->id}}','{{$item->name}}')" class="text-danger">
                                                            <i class="fa fa-trash"></i>
                                                        </span>
                                                </div>
                                                <div title="{{$item->news_type}}" class="@if ($item->news_type == 'publish') border-success alert-success @else border-warning alert-warning @endif" style="position:absolute;margin-top:-12px;margin-left:-12px;width:20px;height:20px;">
                                                    <div style="position:absolute;padding:0 5px;margin-top:-2px;">
                                                        @if ($item->news_type == 'publish')
                                                            P
                                                        @else
                                                            A
                                                        @endif
                                                    </div>
                                                </div>
                                                {{ '['.fdate($item->created_date).'] '.$item->title }}
                                            </td>
                                        
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<form action="{{url('delete-akun')}}" method="POST" class="form-delete">
    @csrf
    <div class="modal fade" id="delete">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-trash text-white bg-danger" style="border:solid 1px #e3342f;padding:3px"></i>
                        Hapus Akun
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <p class="text-center">
                        Yakin akan menghapus akun <span class="text-danger name"></span> ? 
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-update-kontak btn-success"> <i class="fa fa-trash"></i> Hapus Akun</button>
                </div>
            </div>
        </div>
    </div>
</form>



@endsection

@section('css')
    <style>
        .img-hover-opacity:hover{
            opacity: 0.5;
        }
    </style>
@endsection


@section('js')

@if (\Session::has('messge'))
    <script>
        $(document).ready(function(){
            @if (\Session::get("messge")['status'] == true)
                toastr.success('{{\Session::get("messge")["message"]}}');
            @else
                toastr.error('{{\Session::get("messge")["message"]}}');                
            @endif
        })
    </script>
@endif


    <script>
        
        function softDelete(id, name){
            $('#delete input[name=id]').val(id);
            $('#delete .name').html(name);
        }
        
        $(document).ready(function(){
            $.ajax({
                url : '{{url("kecamatan/3528")}}',
                type: 'GET',
                success : function(res){
                    $('select[name=kecamatan]').html('<option value=""> -- Pilih Kecamatan -- </option>');
                    $.each(res.districts, function(index, val){
                        $('select[name=kecamatan]').append('<option value="'+val.id+'">'+val.name+'</option>');
                    })
                }
            })

            $('select[name=kecamatan]').change(function(){
                var data = $(this).val();
                getAjax('{{url("kelurahan")}}/'+data, 'select[name=kelurahan]', 'villages', ' -- Pilih Kelurahan -- ')
            })

            $('.btn-edit').click(function(){
                var id = $(this).attr('id')
                $.ajax({
                    url : '{{url("akun")}}/'+id,
                    type: 'get',
                    success : function(res){
                        $('#edit_kontak input[name=id]').val(res.id)
                        $('#edit_kontak select[name=kecamatan]').val(res.district_id)
                        $('#edit_kontak input[name=call_center]').val(res.call_center)
                        $('#edit_kontak input[name=nama_lengkap]').val(res.name)
                        $('#edit_kontak input[name=username]').val(res.email)
                        $('#edit_kontak select[name=level]').val(res.level)
                        if(res.village_id){
                            getAjax('{{url("kelurahan")}}/'+res.district_id, 'select[name=kelurahan]', 'villages', ' -- Pilih Kelurahan -- ')
                            setTimeout(function(){
                                $('#edit_kontak select[name=kelurahan]').val(res.village_id)                            
                            },100)
                        }
                        if(res.image){
                            $('#edit_kontak .images-preview img').attr('src', '{{url("images/users")}}/'+res.image)
                        }else{
                            $('#edit_kontak .images-preview img').attr('src', '{{url("assets/images/gallery/users.svg")}}')
                        }
                    }
                })
            })

            $('.btn-add').click(function(){
                $('select[name=kecamatan]').val('');
                $('select[name=kelurahan]').val('');
            })
            
            $('.btn-save-akun').click(function(){
                var target = $(this).attr('form-target');
                $.ajax({
                    url : '{{url("check-akun")}}',
                    type : 'post',
                    data : $('.'+target).serialize(),
                    success : function(res){
                        $('.'+target).submit();
                    },
                    error : function(err){
                        $('.form-text').html('');
                        $.each(err.responseJSON.errors, function(index, val){
                            $('.'+target+' .'+index).html(val[0]);
                        })
                    }
                })
            })
        })
        
    </script>
@endsection