@extends('layouts.app')

@section('content')
<div class="container">
<form action="" class="news" reload="5" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="set_feature_images" value="@if($data){{$data->img_thumb}}@endif">
    <input type="hidden" name="news_status" value="archive">
    <input type="hidden" name="actions" value="save">
    <input type="hidden" id="news_id" name="id" value="{{$id}}">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="">
                    <div class="row bg-white pt-3 ml-0 mr-0">
                        <div class="col-2 pt-2">
                            <div class="text-right pt-2" style="font-weight:bold;font-size:15px;color:orange">JUDUL BERITA</div>
                        </div>
                        <div class="col-6 pt-2 p-0">
                            <input type="text" name="title" value="@if($data){{$data->title}}@endif" class="form-control news-title">
                        </div>
                        <div class="col-4 pt-2">
                            <div class="row">
                                <div class="col pl-1 pr-0">
                                    <a title="Daftar berita" href="{{url('news')}}" class="btn btn-block btn-light">
                                        <i class="fa fa-newspaper"></i>
                                    </a>
                                </div>
                                <div title="Arsip" class="col pl-1 pr-0">
                                    <button type="button" class="btn btn-arsip btn-block btn-warning">
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>
                                <div title="Publikasikan" class="col pl-1 pr-0">
                                    <button type="button" class="btn btn-block btn-publikasikan btn-success">
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>
                                <div title="Pratinjau" class="col pl-1 pr-0 pr-4">
                                    <button type="button" class="btn btn-pratinjau btn-block btn-primary">
                                        <i class="fa fa-globe"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row ml-0 mr-0 pr-4">
                        <div class="col-2">
                            <div class="text-muted mb-4 text-right mt-2">
                                <i class="fa fa-link"></i>
                                Hiperlink
                            </div>
                        </div>
                        <div class="col-7 pt-2 pl-1 p-0">
                            <div class="text-muted text-left">
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">{{url('news')}}/</span>
                                    </div>
                                    <input type="text" name="hiperlink" value="@if($data){{$data->link}}@endif" class="form-control news-hiperlink">
                                </div>
                            </div>
                        </div>
                        <div class="col-3 mt-2">
                            <div style="cursor:pointer" data-toggle="modal" data-target="#set-feature-image" class="text-muted set-feature-image text-right mt-1">
                                <i class="fa fa-image"></i>
                                Atur gambar utama
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="margin-top:-8px;cursor:pointer" data-toggle="modal" data-target="#preview-image" class="text-right preview-images text-info">@if($data) {{url('').$data->img_thumb}} @endif</div>
                        </div>
                    </div>
                    
                    

                    <div class="row">
                        <div class="col-12 ajb-editor">
                            <textarea name="contents">@if($data) {{$data->contents}} @endif</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


    <div class="modal fade" id="preview-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title preview-image"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="@if($data) {{url('').$data->img_thumb}} @endif" width="100%" class="preview-img">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="set-feature-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="display:block;">
                    <div class="float-right">
                        <form action="" class="form-ajb" method="post" enctype="multipart/form-data">
                            <input type="file" style="display:none" name="images_ajb" accept="image/*">
                        </form>
                        <i style="cursor:pointer" onclick="return uploadimages()" class="fa fa-plus"></i>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <h5 class="modal-title preview-image">
                        <i class="fa fa-image"></i>
                        Atur gambar utama
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row master-images"></div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('css')
    <link href="{{asset("assets/plugins/wysiwyg-editor-master/css/froala_editor.pkgd.css")}}" rel="stylesheet" type="text/css" />
    <style>
        .set-images:hover{
            opacity: 0.5;
            cursor: pointer;
        }
        .btn-warning{
            color: #ffffff;
            background-color: #f9b115;
            border-color: #f9b115;
        }
        .btn-warning:hover, .btn-warning:focus, .btn-warning.focus {
            color: #fff;
            background-color: #e29c06;
            border-color: #d69405;
        }
    </style>
@endsection

@section('js')
<script type="text/javascript" src="{{asset("assets/plugins/wysiwyg-editor-master/js/froala_editor.pkgd.min.js")}}"></script>
<script type="text/javascript">
    window.onbeforeunload = function() {
        var id = document.getElementById("news_id").value;
        if(id == 'false'){
            return "Da ta tidak akan disimpan atau diabaikan";
        }
    }
</script>
<script>

    function readURLImagesView(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#preview-image .preview-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function uploadimages(){
            $('.form-ajb input').click();

            $('.form-ajb input').on('change', function(){
                $('.form-ajb').submit();
            })

            $('.form-ajb').on('submit', function(e){
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url : '{{url("api/upload-new-images")}}',
                    type : 'post',
                    data : formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success : function(res){
                        $('.fr-desktop .fr-modal-wrapper .fr-modal-head button[title=Cancel]').click();
                        

                        $.ajax({
                            url : '{{url('')}}/api/get-images',
                            type : 'get',
                            success : function(res){
                                $('.master-images').html('');
                                $.each(res, function(index, data){
                                    var template = [
                                        '<div class="col-3">',
                                            '<div>',
                                                '<img class="set-images" width="100%" src="'+data.url+'"/>',
                                            '</div>',
                                        '</div>',
                                    ].join('\n')

                                    $('.master-images').append(template)
                                })

                                $('.set-images').click(function(){
                                    var urlImage = $(this).attr('src');
                                    var splitUrl = urlImage.split('{{url('')}}').join('');
                                    $('#set-feature-image .close').click()
                                    $('#set-feature-image .close').click()
                                    $('#preview-image img').attr('src',urlImage);
                                    $('.preview-images').html('<small><i>'+urlImage+'</i></small>');
                                    $('input[name=set_feature_images]').val(splitUrl);
                                })
                            }
                        })

                    }
                })
            })

    }

    setTimeout(function(){
        var editor = new FroalaEditor('textarea', {
            imageUpload: false,
            videoUpload: false,
            imageManagerLoadURL: '{{url('')}}/api/get-images',
            events: {
                'imageManager.imagesLoaded': function (data) {
                    $('.ajb-images').remove();
                    var temp = [
                        '<form action="" class="form-ajb" method="post" enctype="multipart/form-data">',
                            '<input type="file" style="display:none" name="images_ajb" accept="image/*">',
                        '</form>',
                        '<button type="button" onclick="return uploadimages()" style="border:none" class="btn mt-1 btn-outline-light"> <i class="fa text-success fa-plus"></i> </button>',
                    ].join('\n');
                    $('.fr-desktop .fr-modal-wrapper .fr-modal-head .fr-modal-head-line').append('<div class="float-right ajb-images mr-5 mt-2"> '+temp+' </div>');
                }
            },
        });

        

        

    },100)
    $(document).ready(function(){
        $('.news-title').on('change paste keyup', function(){
            var val = $(this).val().toLowerCase();
            var text = val.split(' ').join('-');
            $('.news-hiperlink').val($.trim(text));
        })

        $('.set-feature-image').click(function(){
            $.ajax({
                url : '{{url('')}}/api/get-images',
                type : 'get',
                success : function(res){
                    $('.master-images').html('');
                    $.each(res, function(index, data){
                        var template = [
                            '<div class="col-3">',
                                '<div>',
                                    '<img class="set-images" width="100%" src="'+data.url+'"/>',
                                '</div>',
                            '</div>',
                        ].join('\n')

                        $('.master-images').append(template)
                    })

                    $('.set-images').click(function(){
                        var urlImage = $(this).attr('src');
                        var splitUrl = urlImage.split('{{url('')}}').join('');
                        $('#set-feature-image .close').click()
                        $('#set-feature-image .close').click()
                        $('#preview-image img').attr('src',urlImage);
                        $('.preview-images').html('<small><i>'+urlImage+'</i></small>');
                        $('input[name=set_feature_images]').val(splitUrl);
                    })
                }
            })
        })

        // setInterval(function() {
        //     var waktu = $('.news').attr('reload');
        //     waktu--;
        //     if(waktu < 0) {
        //         $('.news').attr('reload', '5')
        //         // $('.news').submit()
        //     }else{
        //         $('.news').attr('reload', waktu)
        //     }

        // }, 1000);
        $('.btn-publikasikan').click(function(){
            $('input[name=news_status]').val('publish')
            $('input[name=actions]').val('archive')
            $('.news').submit()
        })
        $('.btn-arsip').click(function(){
            $('input[name=news_status]').val('archive')
            $('input[name=actions]').val('archive')
            $('.news').submit()
        })
        $('.btn-pratinjau').click(function(){
            $('input[name=news_status]').val('archive')
            $('input[name=actions]').val('pratinjau')
            $('.news').submit()
        })
        $('.news').on('submit', function(e){
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url : '{{url("news-update")}}',
                type : 'post',
                data : formData,
                cache:false,
                contentType: false,
                processData: false,
                success : function(res){
                    if(res.status == false){
                        toastr.error(res.message);
                    }else{
                        $('input[name=id]').val(res.id)
                        toastr.success(res.message);
                        if(res.actions == 'pratinjau'){
                            setTimeout(function(){
                                var url = "{{url('blog-preview')}}/" + $.trim($('input[name=hiperlink]').val());
                                window.open(url, '_blank');
                            },500)
                        }
                    }
                }
            })
        })
    })
</script>


@endsection