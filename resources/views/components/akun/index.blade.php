@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-warning">
                <div class="card-header bg-white">
                    <div class="float-right">
                        <button data-toggle="modal" data-target="#add" class="btn btn-sm btn-add btn-outline-success">
                            <i class="fa fa-plus"></i>
                            Tambah Akun
                        </button>
                    </div>
                    <i class="fa fa-users mr-2"></i>
                    Akun users
                </div>

                <div class="card-body">
                    
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class=" text-center">NO</th>
                                    <th class="">Name</th>
                                    <th class="">Email / Username</th>
                                    <th class="">Level</th>
                                    <th class="">Kecamatan/Kelurahan</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($users->count() > 0)
                                    @foreach ($users as $key => $item)
                                        <tr>
                                            <td class="text-center">
                                                <div class="@if ($item->level == 'super admin') border-light alert-light @elseif($item->level == 'admin') border-warning alert-warning @else border-success alert-success @endif" style="position:absolute;margin-top:-12px;margin-left:-12px;width:20px;height:20px;">
                                                    <div style="position:absolute;padding:0 5px;margin-top:-2px;">
                                                        {{ $key + 1 }} 
                                                    </div>
                                                </div>
                                                @if ($item->image)
                                                    <img style="width:40px;height:40px" class="img-thumbnail" src="{{url('images/users/'.$item->image)}}" alt="">
                                                @else
                                                    <img style="width:40px;height:40px" class="img-thumbnail" src="{{url('assets/images/gallery/users.svg')}}" alt="">                                                    
                                                @endif
                                            </td>
                                            <td> 
                                                {{ $item->name }}
                                            </td>
                                            <td> 
                                                @if ($item->level == 'super admin')
                                                    <div class="btn btn-light">{{ $item->email }}</div>
                                                @elseif($item->level == 'admin')
                                                    <div class="btn btn-warning">{{ $item->email }}</div>
                                                @else
                                                    <div class="btn btn-success">{{ $item->email }}</div>                                                    
                                                @endif
                                                
                                            </td>
                                            <td> 
                                                @if ($item->level == 'super admin')
                                                    <div class="btn btn-light">{{ $item->level }}</div>
                                                @elseif($item->level == 'admin')
                                                    <div class="btn btn-warning">{{ $item->level }}</div>
                                                @else
                                                    <div class="btn btn-success">{{ $item->level }}</div>                                                    
                                                @endif
                                            </td>
                                            <td> 
                                                {{ ($item->kecamatan)? 'Kec. '.$item->kecamatan->name.', ' :'' }} 
                                                {{ ($item->kelurahan)? 'Kel/Des. '.$item->kelurahan->name :'' }}
                                                {{ ($item->kecamatan == false && $item->kelurahan == false)? '-':'' }}
                                            </td>
                                            <td class="text-center">
                                                <button data-toggle="modal" data-target="#edit_kontak" id="{{$item->id}}" class="btn btn-edit btn-sm btn-success">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button data-toggle="modal" data-target="#delete" onclick="softDelete('{{$item->id}}','{{$item->name}}')" class="btn btn-sm btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<form action="{{url('add-akun')}}" method="POST" enctype="multipart/form-data" class="form-add">
    @csrf
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-user text-white bg-success" style="-webkit-transform: scaleX(-1);transform: scaleX(-1);border:solid 1px #38c172;padding:3px"></i>
                        Tambah Akun
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col">
                            <label>Kecamatan (opsional)</label>
                            <select name="kecamatan" class="form-control"></select>
                            <small class="form-text kecamatan text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Kelurahan (opsional)</label>
                            <select name="kelurahan" class="form-control">
                                <option value=""> -- Pilih kelurahan -- </option>
                            </select>
                            <small class="form-text kelurahan text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input name="nama_lengkap" type="text" class="form-control" placeholder="Full name">
                        <small class="form-text nama_lengkap text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input name="username" type="email" class="form-control" placeholder="Username/Email">
                        <small class="form-text username text-danger"></small>
                    </div>
                    <div class="row">
                        {{-- col --}}
                        <div class="col-8">
                            <div class="form-group">
                                <label>Password</label>
                                <input name="password" type="email" class="form-control" placeholder="Password">
                                <small class="form-text password text-danger"></small>
                            </div>
                            <div class="form-group">
                                <label>Level</label>
                                <select name="level" class="form-control">
                                    <option value=""> -- Pilih Level -- </option>
                                    <option value="super admin"> Super Admin </option>
                                    <option value="admin"> Admin </option>
                                    <option value="mobile"> Mobile </option>
                                </select>
                                <small class="form-text kelurahan text-danger"></small>
                            </div>
                        </div>
                        {{-- end col --}}
                        <div class="col-4">
                            <div style="margin-top:30px;" class="images-preview" target-form="file-edit">
                                <input type="file" name="images" id="file-edit" target-image="image-edit" style="display:none" accept="image/*">
                                <img style="width:90%;height:120px;cursor:pointer;" id="image-edit" title="Upload Image" class="img-thumbnail img-hover-opacity" src="{{url('assets/images/gallery/users.svg')}}" alt="">
                            </div>
                        </div>
                    </div>
                    {{-- end row --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="button" form-target="form-add" class="btn btn-save-akun btn-success"> <i class="fa fa-save"></i> Simpan kontak</button>
                </div>
            </div>
        </div>
    </div>
</form>


<form action="{{url('update-akun')}}" method="POST" enctype="multipart/form-data" class="form-edit">
    @csrf
    <div class="modal fade" id="edit_kontak" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-phone text-white bg-success" style="-webkit-transform: scaleX(-1);transform: scaleX(-1);border:solid 1px #38c172;padding:3px"></i>
                        Tambah Akun
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <div class="form-group row">
                        <div class="col">
                            <label>Kecamatan (opsional)</label>
                            <select name="kecamatan" class="form-control"></select>
                            <small class="form-text kecamatan text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Kelurahan (opsional)</label>
                            <select name="kelurahan" class="form-control">
                                <option value=""> -- Pilih kelurahan -- </option>
                            </select>
                            <small class="form-text kelurahan text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input name="nama_lengkap" type="text" class="form-control" placeholder="Full name">
                        <small class="form-text nama_lengkap text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input name="username" type="email" class="form-control" placeholder="Username/Email">
                        <small class="form-text username text-danger"></small>
                    </div>
                    <div class="row">
                        {{-- col --}}
                        <div class="col-8">
                            <div class="form-group">
                                <label>Password</label>
                                <input name="password" type="email" class="form-control" placeholder="Password">
                                <small class="form-text password text-danger"></small>
                            </div>
                            <div class="form-group">
                                <label>Level</label>
                                <select name="level" class="form-control">
                                    <option value=""> -- Pilih Level -- </option>
                                    <option value="super admin"> Super Admin </option>
                                    <option value="admin"> Admin </option>
                                    <option value="mobile"> Mobile </option>
                                </select>
                                <small class="form-text kelurahan text-danger"></small>
                            </div>
                        </div>
                        {{-- end col --}}
                        <div class="col-4">
                            <div style="margin-top:30px;" class="images-preview" target-form="file-edit">
                                <input type="file" name="images" id="file-edit" target-image="image-edit" style="display:none" accept="image/*">
                                <img style="width:90%;height:120px;cursor:pointer;" id="image-edit" title="Upload Image" class="img-thumbnail img-hover-opacity" src="{{url('assets/images/gallery/users.svg')}}" alt="">
                            </div>
                        </div>
                    </div>
                    {{-- end row --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="button" form-target="form-edit" class="btn btn-save-akun btn-success"> <i class="fa fa-save"></i> Simpan kontak</button>
                </div>
            </div>
        </div>
    </div>
</form>



<form action="{{url('delete-akun')}}" method="POST" class="form-delete">
    @csrf
    <div class="modal fade" id="delete">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-trash text-white bg-danger" style="border:solid 1px #e3342f;padding:3px"></i>
                        Hapus Akun
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <p class="text-center">
                        Yakin akan menghapus akun <span class="text-danger name"></span> ? 
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-update-kontak btn-success"> <i class="fa fa-trash"></i> Hapus Akun</button>
                </div>
            </div>
        </div>
    </div>
</form>



@endsection

@section('css')
    <style>
        .img-hover-opacity:hover{
            opacity: 0.5;
        }
    </style>
@endsection


@section('js')

@if (\Session::has('messge'))
    <script>
        $(document).ready(function(){
            @if (\Session::get("messge")['status'] == true)
                toastr.success('{{\Session::get("messge")["message"]}}');
            @else
                toastr.error('{{\Session::get("messge")["message"]}}');                
            @endif
        })
    </script>
@endif


    <script>
        
        function softDelete(id, name){
            $('#delete input[name=id]').val(id);
            $('#delete .name').html(name);
        }
        
        $(document).ready(function(){
            $.ajax({
                url : '{{url("kecamatan/3528")}}',
                type: 'GET',
                success : function(res){
                    $('select[name=kecamatan]').html('<option value=""> -- Pilih Kecamatan -- </option>');
                    $.each(res.districts, function(index, val){
                        $('select[name=kecamatan]').append('<option value="'+val.id+'">'+val.name+'</option>');
                    })
                }
            })

            $('select[name=kecamatan]').change(function(){
                var data = $(this).val();
                getAjax('{{url("kelurahan")}}/'+data, 'select[name=kelurahan]', 'villages', ' -- Pilih Kelurahan -- ')
            })

            $('.btn-edit').click(function(){
                var id = $(this).attr('id')
                $.ajax({
                    url : '{{url("akun")}}/'+id,
                    type: 'get',
                    success : function(res){
                        $('#edit_kontak input[name=id]').val(res.id)
                        $('#edit_kontak select[name=kecamatan]').val(res.district_id)
                        $('#edit_kontak input[name=call_center]').val(res.call_center)
                        $('#edit_kontak input[name=nama_lengkap]').val(res.name)
                        $('#edit_kontak input[name=username]').val(res.email)
                        $('#edit_kontak select[name=level]').val(res.level)
                        if(res.village_id){
                            getAjax('{{url("kelurahan")}}/'+res.district_id, 'select[name=kelurahan]', 'villages', ' -- Pilih Kelurahan -- ')
                            setTimeout(function(){
                                $('#edit_kontak select[name=kelurahan]').val(res.village_id)                            
                            },100)
                        }
                        if(res.image){
                            $('#edit_kontak .images-preview img').attr('src', '{{url("images/users")}}/'+res.image)
                        }else{
                            $('#edit_kontak .images-preview img').attr('src', '{{url("assets/images/gallery/users.svg")}}')
                        }
                    }
                })
            })

            $('.btn-add').click(function(){
                $('select[name=kecamatan]').val('');
                $('select[name=kelurahan]').val('');
            })
            
            $('.btn-save-akun').click(function(){
                var target = $(this).attr('form-target');
                $.ajax({
                    url : '{{url("check-akun")}}',
                    type : 'post',
                    data : $('.'+target).serialize(),
                    success : function(res){
                        $('.'+target).submit();
                    },
                    error : function(err){
                        $('.form-text').html('');
                        $.each(err.responseJSON.errors, function(index, val){
                            $('.'+target+' .'+index).html(val[0]);
                        })
                    }
                })
            })
        })
        
    </script>
@endsection