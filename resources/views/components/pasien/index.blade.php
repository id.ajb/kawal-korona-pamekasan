@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-danger">
                <div class="card-header bg-white">
                    <div class="float-right">
                        <button data-toggle="modal" data-target="#add" class="btn btn-sm btn-add btn-outline-danger">
                            <i class="fa fa-plus"></i>
                            Tambah Pasien
                        </button>
                    </div>
                    <i class="fa fa-ambulance mr-2"></i>
                    Daftar Pasien
                </div>

                <div class="card-body">
                    <div class="mb-3">
                        <a href="{{url('pasien-wilayah')}}" class="btn @if(Request::segments()[0] == 'pasien-wilayah') active @endif btn-outline-success">
                            Wilayah
                        </a>
                        <a href="{{url('pasien')}}" class="btn @if(Request::segments()[0] == 'pasien') active @endif btn-outline-info">
                            Pasien
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table id="datatable-custom" class="table table-sm">
                            <thead>
                                <tr>
                                    <th class="text-center">NO</th>
                                    <th class="">Kelurahan</th>
                                    <th class="">NIK</th>
                                    <th class="">Nama</th>
                                    <th class="">L/P</th>
                                    <th class="">Usia</th>
                                    <th class="">Status</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($pasien->count() > 0)
                                    @foreach ($pasien as $key => $item)
                                        <tr>
                                            <td class="text-center">
                                                {{ $key + 1 }}
                                            </td>
                                            <td> 
                                                {{ ($item->kelurahan)? $item->kelurahan->name :'' }}
                                            </td>
                                            <td> 
                                                {{ ($item->nik)? $item->nik :'-' }}
                                            </td>
                                            <td> 
                                                {{ ($item->name)? $item->name :'-' }}
                                            </td>
                                            <td> 
                                                {{ $item->gender}}
                                                
                                            </td>
                                            <td> 
                                               {{ ($item->age)? $item->age.' Thn':'-' }}
                                            </td>
                                            <td>
                                                @if ($item->status_covid == 'ODP')
                                                    <button class="btn btn-sm btn-light">{{$item->status_covid}}</button>
                                                @elseif($item->status_covid == 'PDP')
                                                    <button class="btn btn-sm btn-warning">{{$item->status_covid}}</button>
                                                @elseif($item->status_covid == 'POSITIF AKTIF')
                                                    <button class="btn btn-sm btn-danger">{{$item->status_covid}}</button>
                                                @elseif($item->status_covid == 'POSITIF SEMBUH')
                                                    <button class="btn btn-sm btn-success">{{$item->status_covid}}</button>
                                                @elseif($item->status_covid == 'MENIGGAL')
                                                    <button class="btn btn-sm btn-secondary">{{$item->status_covid}}</button>
                                                @else
                                                    <button class="btn btn-sm btn-light">{{$item->status_covid}}</button>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <button data-toggle="modal" data-target="#edit-pasien" id="{{$item->id}}" class="btn btn-edit btn-sm btn-success">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button data-toggle="modal" data-target="#delete" onclick="softDelete('{{$item->id}}','{{$item->nik}}')" class="btn btn-sm btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<form action="{{url('add-pasien')}}" method="POST" class="form-add">
    @csrf
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-ambulance text-white bg-success" style="border:solid 1px #38c172;padding:3px"></i>
                        Tambah Pasien
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="redirect" value="pasien">
                    <div class="form-group row">
                        <div class="col">
                            <label>Kecamatan <span class="text-danger">*</span></label>
                            <select name="kecamatan" class="form-control"></select>
                            <small class="form-text kecamatan text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Kelurahan <span class="text-danger">*</span></label>
                            <select name="kelurahan" class="form-control">
                                <option value=""> -- Pilih kelurahan -- </option>
                            </select>
                            <small class="form-text kelurahan text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label>NIK <span class="text-danger">*</span></label>
                            <input name="nik" type="text" class="form-control" placeholder="Nama Lengkap">
                            <small class="form-text nik text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Nama (opsional)</label>
                            <input name="nama_lengkap" type="text" class="form-control" placeholder="Nama Lengkap">
                            <small class="form-text nama_lengkap text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label>Tempat Lahir (opsional)</label>
                            <input name="tempat_lahir" type="text" placeholder="Tempat Lahir" class="form-control">
                            <small class="form-text tempat_lahir text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Tanggal Lahir (opsional)</label>
                            <input name="tanggal_lahir" type="date" class="form-control">
                            <small class="form-text tanggal_lahir text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label>Jenis Kelamin <span class="text-danger">*</span></label>
                            <select name="jenis_kelamin" class="form-control">
                                <option value=""> -- Pilih Jenis Kelamin -- </option>
                                <option value="L">Laki-laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                            <small class="form-text jenis_kelamin text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Usia (opsional)</label>
                            <div class="input-group">
                                <input name="usia" type="number" placeholder="Usia" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text">tahun</span>
                                </div>
                            </div>
                            <small class="form-text usia text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Status Covid-19 <span class="text-danger">*</span> </label>
                        <select name="status_covid" class="form-control">
                            <option value=""> -- Pilih Status Covid-19 -- </option>
                            <option value="ODP"> ODP </option>
                            <option value="PDP"> PDP </option>
                            <option value="POSITIF AKTIF"> POSITIF AKTIF </option>
                            <option value="POSITIF SEMBUH"> POSITIF SEMBUH </option>
                            <option value="MENIGGAL"> MENIGGAL </option>
                        </select>
                        <small class="form-text status_covid text-danger"></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="button" form-target="form-add" class="btn btn-save-akun btn-success"> <i class="fa fa-save"></i> Simpan kontak</button>
                </div>
            </div>
        </div>
    </div>
</form>


<form action="{{url('update-pasien')}}" method="POST" enctype="multipart/form-data" class="form-edit">
    @csrf
    <div class="modal fade" id="edit-pasien" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-ambulance text-white bg-success" style="border:solid 1px #38c172;padding:3px"></i>
                        Edit Pasien
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="redirect" value="pasien">
                    <input type="hidden" name="id">
                    <div class="form-group row">
                        <div class="col">
                            <label>Kecamatan <span class="text-danger">*</span></label>
                            <select name="kecamatan" class="form-control"></select>
                            <small class="form-text kecamatan text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Kelurahan <span class="text-danger">*</span></label>
                            <select name="kelurahan" class="form-control">
                                <option value=""> -- Pilih kelurahan -- </option>
                            </select>
                            <small class="form-text kelurahan text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label>NIK <span class="text-danger">*</span></label>
                            <input name="nik" type="text" class="form-control" placeholder="Nama Lengkap">
                            <small class="form-text nik text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Nama (opsional)</label>
                            <input name="nama_lengkap" type="text" class="form-control" placeholder="Nama Lengkap">
                            <small class="form-text nama_lengkap text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label>Tempat Lahir (opsional)</label>
                            <input name="tempat_lahir" type="text" placeholder="Tempat Lahir" class="form-control">
                            <small class="form-text tempat_lahir text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Tanggal Lahir (opsional)</label>
                            <input name="tanggal_lahir" type="date" class="form-control">
                            <small class="form-text tanggal_lahir text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label>Jenis Kelamin <span class="text-danger">*</span></label>
                            <select name="jenis_kelamin" class="form-control">
                                <option value=""> -- Pilih Jenis Kelamin -- </option>
                                <option value="L">Laki-laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                            <small class="form-text jenis_kelamin text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Usia (opsional)</label>
                            <div class="input-group">
                                <input name="usia" type="number" placeholder="Usia" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text">tahun</span>
                                </div>
                            </div>
                            <small class="form-text usia text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Status Covid-19 <span class="text-danger">*</span> </label>
                        <select name="status_covid" class="form-control">
                            <option value=""> -- Pilih Status Covid-19 -- </option>
                            <option value="ODP"> ODP </option>
                            <option value="PDP"> PDP </option>
                            <option value="POSITIF AKTIF"> POSITIF AKTIF </option>
                            <option value="POSITIF SEMBUH"> POSITIF SEMBUH </option>
                            <option value="MENIGGAL"> MENIGGAL </option>
                        </select>
                        <small class="form-text status_covid text-danger"></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="button" form-target="form-edit" class="btn btn-save-akun btn-success"> <i class="fa fa-save"></i> Simpan kontak</button>
                </div>
            </div>
        </div>
    </div>
</form>



<form action="{{url('delete-pasien')}}" method="POST" class="form-delete">
    @csrf
    <div class="modal fade" id="delete">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-trash text-white bg-danger" style="border:solid 1px #e3342f;padding:3px"></i>
                        Hapus Akun
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <p class="text-center">
                        Yakin akan menghapus pasien dengan NIK <span class="text-danger name"></span> ? 
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-success"> <i class="fa fa-trash"></i> Hapus Akun</button>
                </div>
            </div>
        </div>
    </div>
</form>



@endsection

@section('css')
    <style>
        .img-hover-opacity:hover{
            opacity: 0.5;
        }
    </style>
@endsection


@section('js')

@if (\Session::has('messge'))
    <script>
        $(document).ready(function(){
            @if (\Session::get("messge")['status'] == true)
                toastr.success('{{\Session::get("messge")["message"]}}');
            @else
                toastr.error('{{\Session::get("messge")["message"]}}');                
            @endif
        })
    </script>
@endif

    <script>
        
        function softDelete(id, name){
            $('#delete input[name=id]').val(id);
            $('#delete .name').html(name);
        }
        
        $(document).ready(function(){
            $.ajax({
                url : '{{url("kecamatan/3528")}}',
                type: 'GET',
                success : function(res){
                    $('select[name=kecamatan]').html('<option value=""> -- Pilih Kecamatan -- </option>');
                    $.each(res.districts, function(index, val){
                        $('select[name=kecamatan]').append('<option value="'+val.id+'">'+val.name+'</option>');
                    })
                }
            })

            $('select[name=kecamatan]').change(function(){
                var data = $(this).val();
                getAjax('{{url("kelurahan")}}/'+data, 'select[name=kelurahan]', 'villages', ' -- Pilih Kelurahan -- ')
            })

            $('.btn-edit').click(function(){
                var id = $(this).attr('id')
                $.ajax({
                    url : '{{url("pasien")}}/'+id,
                    type: 'get',
                    success : function(res){
                        $('#edit-pasien input[name=id]').val(res.id)
                        $('#edit-pasien select[name=kecamatan]').val(res.district_id)
                        $('#edit-pasien input[name=nik]').val(res.nik)
                        $('#edit-pasien input[name=nama_lengkap]').val(res.name)
                        $('#edit-pasien input[name=tempat_lahir]').val(res.place_of_birth)
                        $('#edit-pasien input[name=tanggal_lahir]').val(res.date_of_birth)
                        $('#edit-pasien select[name=jenis_kelamin]').val(res.gender)
                        $('#edit-pasien input[name=usi]').val(res.age)
                        $('#edit-pasien select[name=status_covid]').val(res.status_covid)
                        if(res.village_id){
                            getAjax('{{url("kelurahan")}}/'+res.district_id, 'select[name=kelurahan]', 'villages', ' -- Pilih Kelurahan -- ')
                            setTimeout(function(){
                                $('#edit-pasien select[name=kelurahan]').val(res.village_id)                            
                            },100)
                        }
                    }
                })
            })

            $('.btn-add').click(function(){
                $('select[name=kecamatan]').val('');
                $('select[name=kelurahan]').val('');
            })
            
            $('.btn-save-akun').click(function(){
                var target = $(this).attr('form-target');
                $.ajax({
                    url : '{{url("check-pasien")}}',
                    type : 'post',
                    data : $('.'+target).serialize(),
                    success : function(res){
                        if(res.status == false){
                            toastr.error(res.message); 
                        } else {
                            $('.'+target).submit();
                        }
                    },
                    error : function(err){
                        $('.form-text').html('');
                        $.each(err.responseJSON.errors, function(index, val){
                            $('.'+target+' .'+index).html(val[0]);
                        })
                    }
                })
            })

        })
        
    </script>
@endsection