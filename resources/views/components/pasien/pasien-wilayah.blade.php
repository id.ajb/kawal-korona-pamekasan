@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-danger">
                <div class="card-header bg-white">
                    <div class="float-right">
                        <button data-toggle="modal" data-target="#add" class="btn btn-sm btn-add btn-outline-danger">
                            <i class="fa fa-plus"></i>
                            Tambah Pasien
                        </button>
                    </div>
                    <i class="fa fa-ambulance mr-2"></i>
                    Daftar Pasien
                </div>

                <div class="card-body">

                    <div class="mb-3">
                        <a href="{{url('pasien-wilayah')}}" class="btn @if(Request::segments()[0] == 'pasien-wilayah') active @endif btn-outline-success">
                            Wilayah
                        </a>
                        <a href="{{url('pasien')}}" class="btn @if(Request::segments()[0] == 'pasien') active @endif btn-outline-info">
                            Pasien
                        </a>
                    </div>

                    
                   


                    <div id="btn-print" class="row">
                        <div class="col-sm-12 col-md-6 pl-3">
                            <div class="focus pull-left"></div>
                            <div class="excel"></div>
                        </div>
                    </div>


                    <div class="table-responsive">
                        @if ($breakdown == true)
                            <table class="table table-sm table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">NO</th>
                                        <th class="">{{$label}}</th>
                                        <th style="width:15%" class="text-center bg-light">ODP</th>
                                        <th style="width:15%" class="text-center bg-warning">PDP</th>
                                        <th style="width:15%" class="text-center text-white bg-danger">Positif Aktif</th>
                                        <th style="width:15%" class="text-center text-white bg-success">Positif Sembuh</th>
                                        <th style="width:15%" class="text-center text-white bg-secondary">Meninggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($wilayah->count() > 0)
                                        @php
                                            $odp = [];
                                            $pdp = [];
                                            $positif = [];
                                            $sembuh = [];
                                            $meninggal = [];
                                        @endphp
                                        @foreach ($wilayah as $key => $item)
                                            @if ($item->pasien->count() > 0)
                                                @foreach ($item->pasien as $p)
                                                    @if ($p->status_covid == 'ODP')
                                                        @php $odp[] = 'y' @endphp
                                                    @elseif($p->status_covid == 'PDP')
                                                        @php $pdp[] = 'y' @endphp
                                                    @elseif($p->status_covid == 'POSITIF AKTIF')
                                                        @php $positif[] = 'y' @endphp
                                                    @elseif($p->status_covid == 'POSITIF SEMBUH')
                                                        @php $sembuh[] = 'y' @endphp
                                                    @elseif($p->status_covid == 'MENIGGAL')
                                                        @php $meninggal[] = 'y' @endphp
                                                    @else
                                                    @endif
                                                @endforeach
                                            @endif
                                            <tr>
                                                <td class="text-center">
                                                    {{ $key + 1 }}
                                                </td>
                                                <td> 
                                                    <a href="?{{$filter}}={{$item->id}}" style="text-decoration:none">
                                                        {{ $item->name }}
                                                    </a>
                                                </td>
                                                <td class="text-center table-light"> 
                                                    {{ (count($odp))? count($odp) : '-' }}
                                                </td>
                                                <td class="text-center table-warning"> 
                                                    {{ (count($pdp))? count($pdp) : '-' }}
                                                </td>
                                                <td class="text-center table-danger"> 
                                                    {{ count($positif)? count($positif) : '-' }}
                                                </td>
                                                <td class="text-center table-success">
                                                    {{ (count($sembuh))? count($sembuh) : '-' }}
                                                </td>
                                                <td class="text-center table-secondary">
                                                    {{ (count($meninggal))? count($meninggal) : '-' }}
                                                </td>
                                            </tr>
                                        @php
                                            $odp = [];
                                            $pdp = [];
                                            $positif = [];
                                            $sembuh = [];
                                            $meninggal = [];
                                        @endphp
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>

                        @else


                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th class="text-center">NO</th>
                                        <th class="">Kelurahan</th>
                                        <th class="">Nama</th>
                                        <th class="">L/P</th>
                                        <th class="">Usia</th>
                                        <th class="text-center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($pasien->count() > 0)
                                        @foreach ($pasien as $key => $item)
                                            <tr>
                                                <td class="text-center">
                                                    {{ $key + 1 }}
                                                </td>
                                                <td> 
                                                    {{ ($item->kelurahan)? $item->kelurahan->name :'' }}
                                                </td>
                                                <td> 
                                                    {{ ($item->name)? $item->name :'-' }}
                                                </td>
                                                <td> 
                                                    {{ $item->gender}}
                                                    
                                                </td>
                                                <td> 
                                                   {{ ($item->age)? $item->age.' Thn':'-' }}
                                                </td>
                                                <td class="text-center">
                                                    @if ($item->status_covid == 'ODP')
                                                        <button class="btn btn-light">{{$item->status_covid}}</button>
                                                    @elseif($item->status_covid == 'PDP')
                                                        <button class="btn btn-warning">{{$item->status_covid}}</button>
                                                    @elseif($item->status_covid == 'POSITIF AKTIF')
                                                        <button class="btn btn-danger">{{$item->status_covid}}</button>
                                                    @elseif($item->status_covid == 'POSITIF SEMBUH')
                                                        <button class="btn btn-success">{{$item->status_covid}}</button>
                                                    @elseif($item->status_covid == 'MENIGGAL')
                                                        <button class="btn btn-secondary">{{$item->status_covid}}</button>
                                                    @else
                                                        <button class="btn btn-light">{{$item->status_covid}}</button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>


                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<form action="{{url('add-pasien')}}" method="POST" class="form-add">
    @csrf
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"> 
                        <i class="fa fa-ambulance text-white bg-success" style="border:solid 1px #38c172;padding:3px"></i>
                        Tambah Pasien
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="redirect" value="pasien-wilayah">
                    <div class="form-group row">
                        <div class="col">
                            <label>Kecamatan <span class="text-danger">*</span></label>
                            <select name="kecamatan" class="form-control"></select>
                            <small class="form-text kecamatan text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Kelurahan <span class="text-danger">*</span></label>
                            <select name="kelurahan" class="form-control">
                                <option value=""> -- Pilih kelurahan -- </option>
                            </select>
                            <small class="form-text kelurahan text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label>NIK <span class="text-danger">*</span></label>
                            <input name="nik" type="text" class="form-control" placeholder="Nama Lengkap">
                            <small class="form-text nik text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Nama (opsional)</label>
                            <input name="nama_lengkap" type="text" class="form-control" placeholder="Nama Lengkap">
                            <small class="form-text nama_lengkap text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label>Tempat Lahir (opsional)</label>
                            <input name="tempat_lahir" type="text" placeholder="Tempat Lahir" class="form-control">
                            <small class="form-text tempat_lahir text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Tanggal Lahir (opsional)</label>
                            <input name="tanggal_lahir" type="date" class="form-control">
                            <small class="form-text tanggal_lahir text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label>Jenis Kelamin <span class="text-danger">*</span></label>
                            <select name="jenis_kelamin" class="form-control">
                                <option value=""> -- Pilih Jenis Kelamin -- </option>
                                <option value="L">Laki-laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                            <small class="form-text jenis_kelamin text-danger"></small>
                        </div>
                        <div class="col">
                            <label>Usia (opsional)</label>
                            <div class="input-group">
                                <input name="usia" type="number" placeholder="Usia" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text">tahun</span>
                                </div>
                            </div>
                            <small class="form-text usia text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Status Covid-19 <span class="text-danger">*</span> </label>
                        <select name="status_covid" class="form-control">
                            <option value=""> -- Pilih Status Covid-19 -- </option>
                            <option value="ODP"> ODP </option>
                            <option value="PDP"> PDP </option>
                            <option value="POSITIF AKTIF"> POSITIF AKTIF </option>
                            <option value="POSITIF SEMBUH"> POSITIF SEMBUH </option>
                            <option value="MENIGGAL"> MENIGGAL </option>
                        </select>
                        <small class="form-text status_covid text-danger"></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i> Close</button>
                    <button type="button" form-target="form-add" class="btn btn-save-akun btn-success"> <i class="fa fa-save"></i> Simpan kontak</button>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('css')
<style>
.btn-rounded{
    border-radius: 50px !important;
}
</style>
@endsection


@section('js')
@if (\Session::has('messge'))
    <script>
        $(document).ready(function(){
            @if (\Session::get("messge")['status'] == true)
                toastr.success('{{\Session::get("messge")["message"]}}');
            @else
                toastr.error('{{\Session::get("messge")["message"]}}');                
            @endif
        })
    </script>
@endif


    <script>
        
        function softDelete(id, name){
            $('#delete input[name=id]').val(id);
            $('#delete .name').html(name);
        }
        
        $(document).ready(function(){
            $.ajax({
                url : '{{url("kecamatan/3528")}}',
                type: 'GET',
                success : function(res){
                    $('select[name=kecamatan]').html('<option value=""> -- Pilih Kecamatan -- </option>');
                    $.each(res.districts, function(index, val){
                        $('select[name=kecamatan]').append('<option value="'+val.id+'">'+val.name+'</option>');
                    })
                }
            })

            $('select[name=kecamatan]').change(function(){
                var data = $(this).val();
                getAjax('{{url("kelurahan")}}/'+data, 'select[name=kelurahan]', 'villages', ' -- Pilih Kelurahan -- ')
            })

            $('.btn-edit').click(function(){
                var id = $(this).attr('id')
                $.ajax({
                    url : '{{url("akun")}}/'+id,
                    type: 'get',
                    success : function(res){
                        $('#edit_kontak input[name=id]').val(res.id)
                        $('#edit_kontak select[name=kecamatan]').val(res.district_id)
                        $('#edit_kontak input[name=call_center]').val(res.call_center)
                        $('#edit_kontak input[name=nama_lengkap]').val(res.name)
                        $('#edit_kontak input[name=username]').val(res.email)
                        $('#edit_kontak select[name=level]').val(res.level)
                        if(res.village_id){
                            getAjax('{{url("kelurahan")}}/'+res.district_id, 'select[name=kelurahan]', 'villages', ' -- Pilih Kelurahan -- ')
                            setTimeout(function(){
                                $('#edit_kontak select[name=kelurahan]').val(res.village_id)                            
                            },100)
                        }
                        if(res.image){
                            $('#edit_kontak .images-preview img').attr('src', '{{url("images/users")}}/'+res.image)
                        }else{
                            $('#edit_kontak .images-preview img').attr('src', '{{url("assets/images/gallery/users.svg")}}')
                        }
                    }
                })
            })

            $('.btn-add').click(function(){
                $('select[name=kecamatan]').val('');
                $('select[name=kelurahan]').val('');
            })
            
            $('.btn-save-akun').click(function(){
                var target = $(this).attr('form-target');
                $.ajax({
                    url : '{{url("check-pasien")}}',
                    type : 'post',
                    data : $('.'+target).serialize(),
                    success : function(res){
                        $('.'+target).submit();
                    },
                    error : function(err){
                        $('.form-text').html('');
                        $.each(err.responseJSON.errors, function(index, val){
                            $('.'+target+' .'+index).html(val[0]);
                        })
                    }
                })
            })
        })


    </script>


@endsection