@extends('layouts.app-guest')

@section('contents')

 <!--Appointment Box-->
    {{-- <section class="appointment-box">
    	<div class="inner-box">
            <div class="cross-icon"><span class="flaticon-cancel"></span></div>
            <div class="title">
                <h2>Get Appointment</h2>
			</div>
			
            <div class="appointment-form">
                <form method="post" action="http://azim.commonsupport.com/korona/contact.html">

                    <div class="form-group">
                        <input type="text" name="text" value="" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" value="" placeholder="Email Address" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" value="" placeholder="Phone no." required>
                    </div>
                    <div class="form-group">
                        <textarea placeholder="Message"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="theme-btn btn-style-one"><span class="txt">Submit now</span></button>
                    </div>
                </form>
            </div>
            
            <div class="contact-info-box">
            	<ul class="info-list">
                	<li><a href="mailto:korona@yousite.com"> korona@yousite.com</a></li>
                    <li><a href="tel:+(000)0000000"> +(000) 000 0000</a></li>
                </ul>
                <ul class="social-list clearfix">
                	<li><a href="#">Facebook</a></li>
                    <li><a href="#">Linkedin</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li><a href="#">Google +</a></li>
                    <li><a href="#">Instagram</a></li>
                </ul>
            </div>
            
        </div>
    </section> --}}
    <!--End Consulting Form-->
	
	<!--Main Slider-->
    <section class="main-slider">
    	
        <div class="main-slider-carousel owl-carousel owl-theme">
            
            <div class="slide">
                <div class="auto-container">
					<div class="clearfix">
						<div class="content">

							@if (isset($login))
								{{-- form login --}}
								<div class="row" style="margin-top:-150px">
									<div class="col-2"></div>
									<div class="col-8">
										<div class="p-5" style="border-radius:10px;background:rgba(255,255,255,0.1);box-shadow:0 0 15px white">
											<form method="POST" action="{{ route('login') }}">
												@csrf
						
												<div class="form-group row">
													<label for="email" class="col-md-12 text-white col-form-label">{{ __('E-Mail Address') }}</label>
													<div class="col-md-12">
														<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Username" value="{{ old('email') }}" required autocomplete="email" autofocus>
						
														@error('email')
															<span class="invalid-feedback" role="alert">
																<strong>{{ $message }}</strong>
															</span>
														@enderror
													</div>
												</div>
						
												<div class="form-group row">
													<label for="password" class="col-md-12 text-white col-form-label">{{ __('Password') }}</label>
													<div class="col-md-12">
														<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="password" name="password" required autocomplete="current-password">
						
														@error('password')
															<span class="invalid-feedback" role="alert">
																<strong>{{ $message }}</strong>
															</span>
														@enderror
													</div>
												</div>
						
						
												<div class="form-group row mt-4">
													<div class="col-md-12">
														<button type="submit" class="btn btn-block btn-primary">
															{{ __('Login') }}
														</button>
													</div>
												</div>
											</form>
										</div>
									</div>
									<div class="col-2"></div>
								</div>

								{{-- end form login --}}
							@else	
								<div>
									<h2>
										Pamekasan Lawan <br>COVID-19 atau Corona Virus
									</h2>
									<div class="text">
										Jika anda mengalami gejala COVID-19 atau Corona Virus, Silahkan hubungi contact darurat berikut :
									</div>
									<div class="link-box">
		
										<a href="#" class="theme-btn btn-style-two">
											<span class="txt"> 
												CALL CENTER 
												<i class="fa fa-phone"></i> 
												@if ($general)
													{{ $general->call_center }}
												@endif
											</span>
										</a>
										<a href="#" class="theme-btn btn-style-three">
											<span class="txt"> 
												<i class="fa fa-phone"></i> 
												@if ($general)
													{{ json_decode($general->hotline,true)[0] }}
												@endif
											</span>
										</a>
									</div>
								</div>
							@endif

						</div>
						<div class="image-box">
							<div class="image">
								<img src="{{asset('assets/images/main-slider/content-image.png')}}" alt="" title="">
							</div>
						</div>
					</div>
                </div>
            </div>            
            <!-- <div class="slide"">
                <div class="auto-container">
					<div class="clearfix">
						<div class="content">
							<h2>Prevention <br>of Coronavirus <br>Disease 2020</h2>
							<div class="text">Maintain at least 1 metre (3 feet) distance between yourself and anyone who is coughing or sneezing.</div>
							<div class="link-box">

								

								<a href="#" class="theme-btn btn-style-two"><span class="txt">Finad a Doctor</span></a>
								<a href="#" class="theme-btn btn-style-three"><span class="txt">learn more</span></a>
							</div>
						</div>
						<div class="image-box">
							<div class="image">
								<img src="{{asset('assets/images/main-slider/content-image-2.png')}}" alt="" title="">
							</div>
						</div>
					</div>
                </div>
            </div> -->
            
        </div>
    </section>
    <!--End Main Slider-->
	
	<!--Features Section One-->
	<section class="features-section-one">
		<div class="auto-container">
			<!--Sec Title-->
			<div class="sec-title centered">
				<div class="title-icon">
					<span class="icon"><img src="{{asset('assets/images/icons/separater.png')}}" alt="" /></span>
				</div>
				<!-- <h2>Contagion Coronavirus</h2> -->
				<h2>Angka Kejadian di Pamekasan</h2>
				<div class="text">
					Update Terakhir: Jumat, 27 Mar 2020 10.00
				</div>
			</div>
			<div class="row clearfix">
				<!-- Feature Block One -->
				<div class="feature-block-one col-lg-4">
					<div class="inner-box wow border-danger fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="content">
							<!-- <div class="icon-box">
								<span class="icon"><img src="{{asset('assets/images/icons/1.png')}}" alt="" /></span>
							</div> -->
							<h3><a href="#" class="text-danger">POSITIF COVID-19</a></h3>
							<p><a href="#">PAMEKASAN</a></p>
							<div class="text-danger" style="font-size:50px">
								{{$covid['positif']}}
							</div>
							<!-- <div class="text">Hands touch many surfaces and can pick up viruses. Once contaminated, hands can transfer the virus.</div> -->
						</div>
					</div>
				</div>
				
				<!-- Feature Block One -->
				<div class="feature-block-one col-lg-4">
					<div class="inner-box wow border-success fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="content">
							<!-- <div class="icon-box">
								<span class="icon"><img src="{{asset('assets/images/icons/2.png')}}" alt="" /></span>
							</div> -->
							<h3><a href="#" class="text-success">SEMBUH</a></h3>
							<p><a href="#">PAMEKASAN</a></p>
							<div class="text-success" style="font-size:50px">
								{{$covid['sembuh']}}
							</div>
							<!-- <h3><a href="#">Air Transmission</a></h3>
							<div class="text">How easily a virus spreads from person-to-person can vary, Some viruses are highly contagious.</div> -->
						</div>
					</div>
				</div>
				
				<!-- Feature Block One -->
				<div class="feature-block-one col-lg-4">
					<div class="inner-box wow border-warning fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="content">
							<!-- <div class="icon-box">
								<span class="icon"><img src="{{asset('assets/images/icons/3.png')}}" alt="" /></span>
							</div> -->
							<h3><a href="#" class="text-warning">MENIGGAL</a></h3>
							<p><a href="#">PAMEKASAN</a></p>
							<div class="text-warning" style="font-size:50px">
								{{$covid['meninggal']}}
							</div>
							<!-- <h3><a href="#">Contaminated Objects</a></h3>
							<div class="text">Restaurants, grocery stores, food processing plants, even your own home—food contamination can happen.</div> -->
						</div>
					</div>
				</div>				
			</div>

			<div class="row clearfix">
				<!-- Feature Block One -->
				
				<!-- Feature Block One -->
				<div class="feature-block-one col-lg-6">
					<div class="inner-box border-success wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="content">
							<h3><a href="#" class="text-success">ODP</a></h3>
							<div>Orang Dalam Pemantauan</div>
							<div class="row">
								<div class="col">
									<h5 class="text-success" style="font-weight:bold">~</h5>
									<p class="text">
										Proses Pemantauan
									</p>
								</div>
								<div class="col">
									<h5 class="text-success" style="font-weight:bold">~</h5>
									<p class="text">
										Selesai Pemantauan
									</p>
								</div>
								<div class="col">
									<h5 class="text-success" style="font-weight:bold">{{$covid['odp']}}</h5>
									<p class="text">
										Total ODP
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="feature-block-one col-lg-6">
					<div class="inner-box border-warning wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="content">
							<h3><a href="#" class="text-warning">PDP</a></h3>
							<div class="text">Pasien Dalam Pengawasan</div>
							<div class="row">
								<div class="col">
									<h5 class="text-warning" style="font-weight:bold">~</h5>
									<p class="text">
										Proses Pemantauan
									</p>
								</div>
								<div class="col">
									<h5 class="text-warning" style="font-weight:bold">~</h5>
									<p class="text">
										Selesai Pemantauan
									</p>
								</div>
								<div class="col">
									<h5 class="text-warning" style="font-weight:bold">{{$covid['pdp']}}</h5>
									<p class="text">
										Total PDP
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>	

			</div>
		</div>
	</section>
	<!--End Features Section-->
	
	<!--Symptoms Section-->
	<section id="about" class="symptoms-section">
		<div class="auto-container">
			<div class="row clearfix">

				<!--Content Column-->
				<div class="content-column col-lg-7 col-md-12 col-sm-12 order-lg-2">
					<div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
						<!--Sec Title-->
						<div class="sec-title">
							<div class="title-icon">
								<span class="icon"><img src="{{asset('assets/images/icons/separater.png')}}" alt="" /></span>
							</div>
							<div class="title">Tentang Coronavirus</div>
							<h2>Gejala Coronavirus</h2>
						</div>
						<div class="text">
							<p>Virus Corona adalah sebuah keluarga virus yang ditemukan pada manusia dan hewan. Sebagian virusnya dapat mengingeksi manusia serta menyebabkan berbagai penyakit, mulai dari penyakit umum seperti flu, hingga penyakit-penyakit yang lebih fatal, seperti Middle East Respiratory Syndrome (MERS) dan Severe Acute Respiratory Syndrome (SARS).</p>
						</div>
						<ul class="list-style-two mb-4">
							<li>Demam</li>
							<li>Batuk Kering</li>
							<li>Sakit Tenggorokan</li>
							<li>Sakit Kepala</li>
							<li>Lemas</li>
							<li>Sesak Nafas</li>
						</ul>
					</div>
				</div>
				
				<!--Image Column-->
				<div class="image-column col-lg-5 col-md-12 col-sm-12">
					<div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							<a href="images/resource/image-2.png')}}" data-fancybox="rule" data-caption=""><img src="{{asset('assets/images/resource/image-2.png')}}" alt="" /></a>
						</div>
					</div>
				</div>
				
				
				
			</div>
		</div>
	</section>
	<!--End Rules Section-->

	<!--Features Section Two-->
	<section class="features-section-two">
		<div class="outer-container">
			<div class="clearfix">
				
				<!--Content Column-->
				<div class="content-column">
					<div class="inner-column">
						<!--Sec Title-->
						<div class="sec-title">
							<div class="title-icon">
								<span class="icon"><img src="{{asset('assets/images/icons/separater.png')}}" alt="" /></span>
							</div>
							<div class="title">How To Protect Ourselves and Others</div>
							<h2>Stay informed and follow advice given by your healthcare provider</h2>
						</div>
						<div class="text">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero perspiciatis sequi delectus, maxime, voluptatum minima nam consectetur explicabo doloremque nihil fugiat quibusdam aut dolor temporibus saepe deserunt id ducimus eaque.</p>
							<p>Libero perspiciatis sequi delectus, maxime, voluptatum minima nam consectetur explicabo doloremque nihil fugiat quibusdam aut dolor.</p>
						</div>
						<!-- List Style One -->
						<ul class="list-style-two">
							<li>Share the latest facts & avoid hyperbole</li>
							<li>Show solidarity with affected people</li>
							<li>Tell the stories of people who have experienced</li>
						</ul>
					</div>
				</div>
				
				<!--Image Column-->
				<div class="image-column wow fadeInRight">
					<div class="inner-column clearfix">
						<div class="big-image">
							<img src="{{asset('assets/images/resource/image-1.png')}}" alt="" />
							<div class="small-image">
								<img src="{{asset('assets/images/resource/image-5.png')}}" alt="" />
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!--End Seo Section-->

	<!--Features Section Four-->
	<section id="prevention" class="features-section-four">
		<div class="auto-container">
			<!--Sec Title-->
			<div class="sec-title centered">
				<div class="title-icon">
					<span class="icon"><img src="{{asset('assets/images/icons/separater.png')}}" alt="" /></span>
				</div>
				<h2>Prevention Coronavirus</h2>
				<div class="text">Consequatur molestiae, eligendi molestias ratione voluptas aliquid praesentium, dolorem doloribus, deleniti <br>officia numquam optio sunt eveniet consequuntur laboriosam at non ullam provident</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-12">
					<!-- Services Block Four -->
					<div class="feature-block-four">
						<div class="inner-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="content p-0">
								<table class="table table-sm table-wilayah table-bordered">
								</table>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!--End Features Section-->


	<!--Features Section Five-->
	<section id="call-center" class="features-section-five">
		<!--Title Box-->
		<div class="title-box" style="">
			<div class="auto-container">
				<h2>HUBUNGI CALL CENTER</h2>
				<div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro quisquam, in mollitia. Adipisci laboriosam ratione alias consectetur assumenda vel qui reprehenderit</div>
			</div>
		</div>
		
		<div class="auto-container">
			<div class="inner-container">
				<div class="row clearfix">
					
					<div class="feature-block-five col-lg-12 col-md-12 col-sm-12">
						<div class="inner-box text-left wow fadeInUp pl-5 pr-5" data-wow-delay="0ms" data-wow-duration="1500ms">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width:50px" class="text-center">NO</th>
										<th style="width:250px">Kecamatan</th>
										<th class="bg-info text-white text-center">Call Center</th>
										<th class="bg-warning text-white">Hotline</th>
									</tr>
								</thead>
								<tbody>
									@if ($kontak->count() > 0)
										@foreach ($kontak as $keys => $k)
											<tr>
												<td class="text-center"> {{ $keys + 1}} </td>
												<td> {{$k->kecamatan->name}} </td>
												<td class="text-center"> {{ ($k->call_center)?$k->call_center:'-' }} </td>
												<td>
													@foreach (json_decode($k->hotline, true) as $kjs)
														<div class="btn btn-light"> {{ $kjs }} </div>
													@endforeach
												</td>
											</tr>
										@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
		</div>
	</section>
	<!--End Features Section Five-->
	
	<!-- Hand Wash Process Section -->
	<section class="hand-wash-process-section">
		<div class="auto-container">
			<!--Sec Title-->
			<div class="sec-title centered">
				<div class="title-icon">
					<span class="icon"><img src="{{asset('assets/images/icons/separater.png')}}" alt="" /></span>
				</div>
				<h2>Hand Wash Process</h2>
				<div class="text">Going forward new normal that has evolved from generation the runway heading streamlined cloud solution <br> user generated content in real-time will have multiple touchpoints for offshoring</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 process-block">
					<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image"><img src="{{asset('assets/images/resource/process-1.png')}}" alt=""></div>
						<h4>Water and Soap</h4>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 process-block">
					<div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
						<div class="image"><img src="{{asset('assets/images/resource/process-2.png')}}" alt=""></div>
						<h4>Palm to Palm</h4>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 process-block">
					<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image"><img src="{{asset('assets/images/resource/process-3.png')}}" alt=""></div>
						<h4>Between Fingers</h4>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 process-block">
					<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image"><img src="{{asset('assets/images/resource/process-4.png')}}" alt=""></div>
						<h4>Focus on Thumbs</h4>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 process-block">
					<div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
						<div class="image"><img src="{{asset('assets/images/resource/process-5.png')}}" alt=""></div>
						<h4>Back to Hands</h4>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 process-block">
					<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image"><img src="{{asset('assets/images/resource/process-6.png')}}" alt=""></div>
						<h4>Focus on Wrist</h4>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!--News Section-->
	<section id="blog" class="news-section">
		<div class="auto-container">
			<!--Sec Title-->
			<div class="sec-title centered">
				<div class="title-icon">
					<span class="icon"><img src="{{asset('assets/images/icons/separater.png')}}" alt="" /></span>
				</div>
				<h2>Latest Update Coronavirus</h2>
				<div class="text">Consequatur molestiae, eligendi molestias ratione voluptas aliquid praesentium, dolorem doloribus, deleniti <br>officia numquam optio sunt eveniet consequuntur laboriosam at non ullam provident</div>
			</div>
			<div class="row clearfix">
				
				@if ($blog->count() > 0)
					@foreach ($blog as $item)
						<div class="news-block col-lg-4 col-md-6 col-sm-12">
							<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">
								<div class="image">
									<a><img src="{{url($item->img_thumb)}}" alt="" /></a>
								</div>
								<div class="lower-content">
									<ul class="post-meta">
										<li>{{ fdate($item->created_date) }}</li>
										<li>By Admin</li>
									</ul>
									<h3><a href="{{url('blog/'.$item->link)}}">{{ $item->title }}</a></h3>
									{{-- <a href="{{url('blog/'.$item->link)}}" class="read-more">Read More <span class="arrow flaticon-next-5"></span></a> --}}
								</div>
							</div>
						</div>
					@endforeach
				@endif
				
			</div>
		</div>
	</section>
	<!--End News Section-->
	



	<script src="{{asset('assets/js/jquery.js')}}"></script>
	<script>
		$(document).ready(function(){
			$.ajax({
				url : '{{url("get-pasien-by-wilayah")}}',
				type : 'get',
				success : function(res){
					$('.table-wilayah').html('')
					var th = [
						'<tr>',
							'<th class="text-center">NO</th>',
							'<th>'+res.label+'</th>',
							'<th style="width:15%" class="text-center bg-light">ODP</th>',
							'<th style="width:15%" class="text-center bg-warning">PDP</th>',
							'<th style="width:15%" class="text-center text-white bg-danger">Positif Aktif</th>',
							'<th style="width:15%" class="text-center text-white bg-success">Positif Sembuh</th>',
							'<th style="width:15%" class="text-center text-white bg-secondary">Meninggal</th>',
						'</tr>'	
					].join('\n');
					$('.table-wilayah').html(th)					
					$.each(res.wilayah, function(index, val){
						var odp = [];
						var pdp = [];
						var positif = [];
						var sembuh = [];
						var meninggal = [];
						if(val.pasien != false){
							$.each(val.pasien, function(key, p){
								if(p.status_covid == 'ODP'){
									odp.push('y')
								} else if (p.status_covid == 'PDP') {
									pdp.push('y')
								} else if (p.status_covid == 'POSITIF AKTIF') {
									positif.push('y')
								} else if (p.status_covid == 'POSITIF SEMBUH') {
									sembuh.push('y')
								} else if (p.status_covid == 'MENIGGAL') {
									meninggal.push('y')
								}else{

								}
							})
							// console.log(odp.length);
						}
						var sumodp = (odp.length != 0)? odp.length : '-';
						var sumpdp = (pdp.length != 0)? pdp.length : '-';
						var sumpositif = (positif.length != 0)? positif.length : '-';
						var sumsembuh = (sembuh.length != 0)? sembuh.length : '-';
						var summeninggal = (meninggal.length != 0)? meninggal.length : '-';
						var tmp = [
							'<tr>',
								'<td class="text-center"> '+(index + 1)+' </td>',
								'<td><a link="{{url("get-pasien-by-wilayah")}}?'+res.filter+'='+val.id+'" class="link-data-pasien text-info" style="text-decoration:none"> '+val.name+' </a></td>',
								'<td class="text-center table-light"> '+ sumodp +' </td>',
								'<td class="text-center table-warning"> '+ sumpdp +' </td>',
								'<td class="text-center table-danger"> '+ sumpositif +' </td>',
								'<td class="text-center table-success"> '+ sumsembuh +' </td>',
								'<td class="text-center table-secondary"> '+ summeninggal +' </td>',
							'</tr>',
						].join('\n');
						$('.table-wilayah').append(tmp);

						var odp = [];
						var pdp = [];
						var positif = [];
						var sembuh = [];
						var meninggal = [];
					})


					$('.link-data-pasien').click(function(){
						var url = $(this).attr('link')

						
						$.ajax({
							url : url,
							type : 'get',
							success : function(res){
								$('.table-wilayah').html('')
								var th = [
									'<tr>',
										'<th class="text-center">NO</th>',
										'<th>'+res.label+'</th>',
										'<th style="width:15%" class="text-center bg-light">ODP</th>',
										'<th style="width:15%" class="text-center bg-warning">PDP</th>',
										'<th style="width:15%" class="text-center text-white bg-danger">Positif Aktif</th>',
										'<th style="width:15%" class="text-center text-white bg-success">Positif Sembuh</th>',
										'<th style="width:15%" class="text-center text-white bg-secondary">Meninggal</th>',
									'</tr>'	
								].join('\n');
								$('.table-wilayah').html(th)					
								$.each(res.wilayah, function(index, val){
									var odp = [];
									var pdp = [];
									var positif = [];
									var sembuh = [];
									var meninggal = [];
									if(val.pasien != false){
										$.each(val.pasien, function(key, p){
											if(p.status_covid == 'ODP'){
												odp.push('y')
											} else if (p.status_covid == 'PDP') {
												pdp.push('y')
											} else if (p.status_covid == 'POSITIF AKTIF') {
												positif.push('y')
											} else if (p.status_covid == 'POSITIF SEMBUH') {
												sembuh.push('y')
											} else if (p.status_covid == 'MENIGGAL') {
												meninggal.push('y')
											}else{

											}
										})
										// console.log(odp.length);
									}
									var sumodp = (odp.length != 0)? odp.length : '-';
									var sumpdp = (pdp.length != 0)? pdp.length : '-';
									var sumpositif = (positif.length != 0)? positif.length : '-';
									var sumsembuh = (sembuh.length != 0)? sembuh.length : '-';
									var summeninggal = (meninggal.length != 0)? meninggal.length : '-';
									var tmp = [
										'<tr>',
											'<td class="text-center"> '+(index + 1)+' </td>',
											'<td><a link="{{url("get-pasien-by-wilayah")}}?'+res.filter+'='+val.id+'" class="link-data-pasien" style="text-decoration:none"> '+val.name+' </a></td>',
											'<td class="text-center table-light"> '+ sumodp +' </td>',
											'<td class="text-center table-warning"> '+ sumpdp +' </td>',
											'<td class="text-center table-danger"> '+ sumpositif +' </td>',
											'<td class="text-center table-success"> '+ sumsembuh +' </td>',
											'<td class="text-center table-secondary"> '+ summeninggal +' </td>',
										'</tr>',
									].join('\n');
									$('.table-wilayah').append(tmp);

									var odp = [];
									var pdp = [];
									var positif = [];
									var sembuh = [];
									var meninggal = [];
								})


							}
						})


					})


				}
			})

			
			
		})
	</script>
	
@endsection