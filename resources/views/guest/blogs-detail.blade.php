@extends('layouts.app-guest')


@section('contents')		
	<!--Page Title-->
	<section class="page-title" style="padding: 50px 0px 50px;">
		<div class="auto-container">
		</div>
	</section>
	<!--End Page Title-->
	
	<!--Sidebar Page Container-->
	<div class="sidebar-page-container p-0" >
		<div class="auto-container">
			<div class="row">            	
				<!--Content Side-->
				<div class="content-side col-lg-12">
					<!--Blog Single-->
					<div class="blog-single" style="border:none">
						<div class="inner-box">
							<div class="lower-content">

								@if ($blog)
									
									<h1 class="mb-0 pb-0 text-dark">{{$blog->title}}</h1>
									<div class="text-muted mb-5">
										{{fdate($blog->created_date). ' '. fdate($blog->created_date, 'h:i')}} WIB
									</div>
									<div class="text">
										{!! $blog->contents !!}
									</div>
								@else
									<h3 class="text-center pt-5 pb-5 mt-5 mb-5">
										Mohon Maaf, halaman tidak tersedia.
									</h3>
								@endif
								
								{{-- <div class="post-share-options">
									<div class="tags"><span>Author: </span><a href="#">Coronavirus,</a> <a href="#">Prevention,</a> <a href="#">Contagion,</a></div>
								</div>
								<div class="post-share-options">
									<div class="tags"><span>Tags: </span><a href="#">Coronavirus,</a> <a href="#">Prevention,</a> <a href="#">Contagion,</a></div>
								</div> --}}
							</div>
						</div>
						
						
						
					</div>
				</div>				
						
			</div>
		</div>
	</div>


	<script src="{{asset('assets/js/jquery.js')}}"></script>
	<script>
		$(document).ready(function(){
			$('li').click(function(){
				var val = $(this).children().attr('href');
				window.location.replace("{{url('')}}" + val);
				
			})
		})
	</script>
	
@endsection
