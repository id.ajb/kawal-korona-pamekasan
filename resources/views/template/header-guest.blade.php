<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>korona - Coronavirus Medical Prevention HTML Template | Homepage</title>
	<link rel="manifest" href="{{asset('assets/manifest.json')}}">
	<!-- Stylesheets -->
	<link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
	<link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet">
	<link href="{{asset('assets/css/color.css')}}" rel="stylesheet">
	<link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
	<link rel="icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>

    <div id="home"></div>
 	
    <!-- Main Header-->
    <header class="main-header">
    	
    	<!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="clearfix">
                	
                	<div class="pull-left logo-box">
                    	<div class="logo"><a href="{{url('')}}"><img src="{{asset('assets/images/logo.png')}}" alt="" title=""></a></div>
                    </div>
                   	
                   	<div class="nav-outer clearfix">
                    
						<!-- Main Menu -->
						<nav class="main-menu navbar-expand-md">
							<div class="navbar-header">
								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>

							<div class="navbar-collapse collapse clearfix scroll-nav" id="navbarSupportedContent">
								<ul class="navigation clearfix">
									@guest
									@else
									<li><a href="{{url('home')}}">Dasboard Admin</a></li>
									@endguest
									<li><a href="#home">Beranda</a></li>
									<li><a href="#about">Covid-19</a></li>
									<li><a href="#prevention">Data</a></li>
									<!-- <li><a href="#team">Team</a></li> -->
									<li><a href="#call-center">Kontak</a></li>
									<li><a href="#blog">Berita</a></li>
									<!-- <li class="dropdown"><a href="#">Page</a>
										<ul>
											<li><a href="index-2.html">Home One</a></li>
											<li><a href="index-3.html">Home Two</a></li>
											<li><a href="index-4.html">Home Three</a></li>
										</ul>
									</li> -->
								</ul>
							</div>
							
						</nav>
						
						<!--Option Box-->
						<div class="option-box">
							
							<!--Search Box-->
							{{-- <div class="search-box-outer">
								<div class="dropdown">
									<button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-search"></span></button>
									<ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu1">
										<li class="panel-outer">
											<div class="form-container">
												<form method="post" action="http://azim.commonsupport.com/korona/blog.html">
													<div class="form-group">
														<input type="search" name="field-name" value="" placeholder="Search Here" required>
														<button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
													</div>
												</form>
											</div>
										</li>
									</ul>
								</div>
							</div>
							
							<div class="nav-btn"><span class="icon flaticon-menu"></span></div> --}}
							
							@guest
							@else
								<a class="text-danger" style="position: relative;
								float: left;
								font-size: 22px;
								margin-left: 22px;
								cursor: pointer;
								top: -3px;" href="{{ route('logout') }}"
								onclick="event.preventDefault();
												document.getElementById('logout-form').submit();">
									<span class="fa fa-sign-out"></span>
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
							@endguest


						</div>
						
					</div>
                   
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
    </header>
    <!--End Main Header -->
	
	<!--Form Back Drop-->
    <div class="form-back-drop"></div>
  