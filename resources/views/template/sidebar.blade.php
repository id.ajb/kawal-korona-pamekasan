<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-md-down-none">
      {{-- <svg class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo">
        <use xlink:href="{{asset('assets/brand/coreui-base.svg')}}"></use>
      </svg> --}}
      <img src="{{asset('assets/brand/coreui-base.svg')}}" class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo">
      <img src="{{asset('assets/brand/coreui-signet.svg')}}" class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
      {{-- <svg class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
        <use xlink:href="{{ env('APP_URL', '') }}/assets/brand/coreui-signet.svg"></use>
      </svg> --}}
    </div>
    
    <ul class="c-sidebar-nav">
      <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('home')}}">
        <span class="c-sidebar-nav-icon mb-2">
          <span class="fa fa-tachometer-alt"></span>
        </span> Dashboard</a>
      </li>
      
      @if (auth()->user()->level == 'super admin' 
      || auth()->user()->level == 'admin')
      <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('news')}}">
        <span class="c-sidebar-nav-icon mb-2">
          <span class="fa fa-newspaper"></span>
        </span> Post Berita</a>
      </li>
      <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="{{url('kontak')}}">
          <span class="c-sidebar-nav-icon mb-2">
            <span class="fa fa-phone" style="-webkit-transform: scaleX(-1);transform: scaleX(-1);"></span>
          </span> Kontak
        </a>
      </li>
      @endif
      <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="{{url('pasien')}}">
          <span class="c-sidebar-nav-icon mb-2">
            <span class="fa fa-ambulance"></span>
          </span> Pasien
        </a>
      </li>
      @if (auth()->user()->level == 'super admin')
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('akun')}}">
          <span class="c-sidebar-nav-icon mb-2">
            <span class="fa fa-users"></span>
          </span> Akun</a>
        </li>
        
      @endif
      {{-- <li class="c-sidebar-nav-dropdown"><a class="c-sidebar-nav-dropdown-toggle" href="#">
        <span class="c-sidebar-nav-icon mb-2">
          <span class="fa fa-cogs"></span>
        </span> Setting</a>
        <ul class="c-sidebar-nav-dropdown-items">
          <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="#pengajuan"> <i class="fa fa-file-contract mr-2"></i> Pengajuan</a></li>
          <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="#pembayaran"> <i class="fa fa-receipt mr-2"></i> Pembayaran</a></li>
          <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="#riwayat"> <i class="fa fa-history mr-2"></i> Riwayat Transaksi</a></li>
        </ul>
      </li> --}}
      {{-- <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" target="_bank" href="{{url('')}}">
          <span class="c-sidebar-nav-icon mb-2">
            <span class="fa fa-user-tie"></span>
          </span> Landing
        </a>
      </li> --}}
    </ul>
    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
    {{-- <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-unfoldable"></button> --}}
  </div>