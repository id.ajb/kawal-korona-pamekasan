	<!--Main Footer-->
	<footer class="main-footer">
      <!--Widgets Section-->
      <div class="auto-container">
        
        <!--Widgets Section-->
              <div class="widgets-section">
                <div class="row clearfix">
                    
                      <!--Column-->
                      <div class="big-column col-lg-6 col-md-12 col-sm-12">
              <div class="row clearfix">
                
                <!--Footer Column-->
                              <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                  <div class="widget widget_about">
                    <div class="logo">
                      <a href="index-2.html"><img src="{{asset('assets/images/logo.png')}}" alt="" /></a>
                    </div>
                    <div class="text">Lorem ipsum dolor amet consectetur adipielit sed eiusm tempor incididunt ut labore dolore.</div>
                    
                  </div>
                </div>
                
                <!--Footer Column-->
                              <div class="footer-column col-lg-6 col-md-6 col-sm-12 pl-lg-5">
                                  <div class="widget widget_links">
                    <h2>About Us</h2>
                    <ul class="list">
                      <li><a href="#">About</a></li>
                      <li><a href="#">Faq's</a></li>
                      <li><a href="#">Team</a></li>
                      <li><a href="#">Prevention</a></li>
                      <li><a href="#">Contact</a></li>
                    </ul>
                  </div>
                </div>
                
              </div>
            </div>
            
            <!--Column-->
                      <div class="big-column col-lg-6 col-md-12 col-sm-12">
              <div class="row clearfix">
                
                <!--Footer Column-->
                              <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                  <div class="widget widget_links">
                    <h2>Usefull Links</h2>
                    <ul class="list">
                      <li><a href="#">Situation Reports</a></li>
                      <li><a href="#">Advice For Public</a></li>
                      <li><a href="#">Prevention</a></li>
                      <li><a href="#">Coronavirus Symptoms</a></li>
                      <li><a href="#">Donor & Partners</a></li>
                    </ul>
                  </div>
                </div>
                
                <!--Footer Column-->
                              <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                  <div class="widget widget_contact">
                    <h2>Contact us</h2>
                    <div class="widget-content">
                      <div class="text">3407  Wilkinson Court, Denver <br> Colorado 33901</div>
                      <ul class="list">
                        <li>Phone: <a href="tel:+17204682299">+1 720-468-2299</a></li>
                        <li>Email: <a href="mailto:support@korona.com">support@korona.com</a></li>
                      </ul>
                      <ul class="social-icon-one clearfix">
                        <li><a href="#" class="fa fa-facebook"></a></li>
                        <li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-google-plus"></a></li>
                        <li><a href="#" class="fa fa-linkedin"></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <div class="auto-container">
          <div class="copyright tex-center">Copyrights 2020 All Rights Reserved.</div>
        </div>
      </div>
    </footer>
  
    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-right-arrow"></span></div>
    
  </div>
  <!--End pagewrapper-->
  
  <script src="{{asset('assets/js/jquery.js')}}"></script>
  <script src="{{asset('assets/js/popper.min.js')}}"></script>
  <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/js/jquery.fancybox.js')}}"></script>
  <script src="{{asset('assets/js/appear.js')}}"></script>
  <script src="{{asset('assets/js/owl.js')}}"></script>
  <script src="{{asset('assets/js/wow.js')}}"></script>
  <script src="{{asset('assets/js/jquery-ui.js')}}"></script>
  <script src="{{asset('assets/js/pagenav.js')}}"></script>
  <script src="{{asset('assets/js/script.js')}}"></script>
  
  {{-- <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXX2jiLItfHCWBqxt9j96zINOUh4LYlUORxhSlCaiDdpmKpd8uOb1SNbuuZrZoF8XCwBo0RU5NMU8Eyq3ZSZP41YiLN%2fQORgX%2fg%2fMZzSLpXjazXG%2fCtNBBqcWyzg44UUyxaxU15f%2bklwEc9JbhZiWz%2fdcFT7gSLv%2bxKSt9ZYdEbq1sKkNMXriEB0rvJGIZAHr6qKGPDSrX3lRmgAJ1GTdWBiGtY4z7HDaipmHyG6o2NjochEyW%2beN47sCy3MQl2qAmLWNgnguu50pnL%2f%2b%2bKBoukedV9hYK0g1DzA6ueri3wxEmf5BaCDEWHPDd%2fZe7HVW5ToauuH8Fu2WwQziLwhpyFbPaXhbAKwFNhBuKj9%2bNXTsWiMMOYaD5RsNpGIrscuGjki%2fCUQnoZHA76F8L0LUigIvWca%2fB06pj%2bRdyw28k3mDQApDkQ8bRZNpabYVPEn27uQr76UFkNZzfwHCY93nBHpyWx6PNYqr5LJdxu2gz3R1f9%2f%2fY4ftpCw%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body> --}}
  </html>