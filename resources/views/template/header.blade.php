
      <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        <button class="c-header-toggler c-class-toggler d-lg-none mr-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
          <span class="c-header-toggler-icon"></span>
        </button>
        <a class="c-header-brand d-sm-none" href="#">
          <img class="c-header-brand" src="{{ asset('') }}assets/brand/coreui-base.svg" width="97" height="46" alt="CoreUI Logo">
        </a>
        <button class="c-header-toggler c-class-toggler ml-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
          <span class="c-header-toggler-icon"></span>
        </button>
        
        <ul class="c-header-nav ml-auto mr-4">
          <li class="c-header-nav-item">
            <a class="c-header-nav-link" target="_blank" href="{{url('')}}">
              <span class="fa fa-globe"></span>  
            </a>
          </li>
          {{-- <li class="c-header-nav-item d-md-down-none mx-2">
            <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              <span class="fa fa-bell"></span>  
              <span class="badge badge-pill badge-danger">5</span>  
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg pt-0">
              <div class="dropdown-header bg-light"><strong>You have 5 notifications</strong></div><a class="dropdown-item d-block" href="#">
              <div class="small mb-1">Upgrade NPM &amp; Bower<span class="float-right"><strong>0%</strong></span></div><span class="progress progress-xs">
              <div class="progress-bar bg-info" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
              </span>
              </a><a class="dropdown-item d-block" href="#">
              <div class="small mb-1">ReactJS Version<span class="float-right"><strong>25%</strong></span></div><span class="progress progress-xs">
              <div class="progress-bar bg-danger" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
              </span>
              </a><a class="dropdown-item d-block" href="#">
              <div class="small mb-1">VueJS Version<span class="float-right"><strong>50%</strong></span></div><span class="progress progress-xs">
              <div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
              </span>
              </a><a class="dropdown-item d-block" href="#">
              <div class="small mb-1">Add new layouts<span class="float-right"><strong>75%</strong></span></div><span class="progress progress-xs">
              <div class="progress-bar bg-info" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
              </span>
              </a><a class="dropdown-item d-block" href="#">
              <div class="small mb-1">Angular 8 Version<span class="float-right"><strong>100%</strong></span></div><span class="progress progress-xs">
              <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
              </span>
              </a><a class="dropdown-item text-center border-top" href="#"><strong>View all tasks</strong></a>
            </div>
          </li> --}}
          <li class="c-header-nav-item dropdown">
            <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              <div class="c-avatar">
                  @if (auth()->user()->image)
                      <img style="width:40px;height:40px" class="c-avatar-img" src="{{url('images/users/'.auth()->user()->image)}}" alt="">
                  @else
                      <img style="width:40px;height:40px" class="c-avatar-img" src="{{url('assets/images/gallery/users.svg')}}" alt="">                                                    
                  @endif
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0">
              <div class="dropdown-divider"></div><a class="dropdown-item" href="#">
                <svg class="c-icon mr-2">
                  <use xlink:href="{{ asset('assets') }}/icons/sign-language.svg#cil-account-logout"></use>
                </svg><form action="/logout" method="POST"> @csrf <button type="submit" class="btn btn-block">Logout</button></form></a>
            </div>
          </li>
        </ul>
        <div class="c-subheader px-3">
          <ol class="breadcrumb border-0 m-0 text-capitalize" style="background:none;">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <?php $segments = ''; ?>
            @for($i = 1; $i <= count(Request::segments()); $i++)
                <?php $segments .= '/'. Request::segment($i); ?>
                @if($i < count(Request::segments()))
                    <li class="breadcrumb-item">{{ str_replace('_',' ',str_replace('-',' ',Request::segment($i))) }}</li>
                @else
                    <li class="breadcrumb-item active">{{ str_replace('_',' ',str_replace('-',' ',Request::segment($i))) }}</li>
                @endif
            @endfor
          </ol>
        </div>
    </header>